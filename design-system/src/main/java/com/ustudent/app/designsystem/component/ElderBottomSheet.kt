package com.ustudent.app.designsystem.component

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ustudent.app.designsystem.icon.UstudentIcons
import com.ustudent.app.designsystem.theme.ElderTheme
import com.ustudent.designsystem.R

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ElderBottomSheet(
    sheetContent: @Composable () -> Unit,
    sheetState: ModalBottomSheetState,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    ModalBottomSheetLayout(
        sheetContent = {
            Icon(
                painter = painterResource(
                    UstudentIcons.BottomSheetLine
                ),
                contentDescription = null,
                modifier = Modifier
                    .padding(top = 8.dp)
                    .fillMaxWidth(),
                tint = colorResource(id = R.color.grey_divider)
            )
            Box(modifier = modifier) {
                sheetContent()
            }
        },
        sheetState = sheetState,
        sheetShape = ElderTheme.bottomSheetShape,
        sheetBackgroundColor = colorResource(id = R.color.background_bottom_sheet),
        scrimColor = Color.Black.copy(alpha = 0.32f)
    ) {
        content()
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Preview
@Composable
fun BottomSheetPreview() {
    ElderTheme {
        val sheetState =
            rememberModalBottomSheetState(initialValue = ModalBottomSheetValue.Expanded)
        ElderBottomSheet(sheetContent = {

        }, sheetState = sheetState) {
            Surface {

            }
        }
    }
}