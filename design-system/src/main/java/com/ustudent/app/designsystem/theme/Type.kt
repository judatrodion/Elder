package com.ustudent.app.designsystem.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.ustudent.designsystem.R

val Typography = Typography(
    body1 = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        letterSpacing = 0.1.sp,
        lineHeight = 24.sp,
        fontSize = 16.sp
    ),
    body2 = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 14.sp
    ),
    h1 = TextStyle(
        fontSize = 24.sp,
        fontWeight = FontWeight.Bold,
        fontFamily = FontFamily(
            Font(
                R.font.oswald
            )
        ),
        textAlign = TextAlign.Center,
    ),
    button = TextStyle(
        fontFamily = FontFamily(
            Font(
                R.font.oswald
            )
        ),
        fontSize = 14.sp,
        letterSpacing = 1.sp,
        lineHeight = 16.sp
    ),
    caption = TextStyle(
        fontWeight = FontWeight.Medium,
        letterSpacing = 0.sp,
        fontSize = 14.sp,
        lineHeight = 16.sp
    ),
    subtitle1 = TextStyle(
        fontFamily = FontFamily(
            Font(
                R.font.oswald
            )
        ),
        fontWeight = FontWeight.Normal,
        letterSpacing = 1.sp,
        lineHeight = 16.sp,
        fontSize = 16.sp
    )
)

val BottomNavigationTextStyle = TextStyle(
    fontSize = 12.sp,
    fontWeight = FontWeight.Normal,
    fontFamily = FontFamily(
        Font(
            R.font.oswald
        )
    )
)

val ButtonTextStyle = TextStyle(
    fontFamily = FontFamily(
        Font(
            R.font.oswald
        )
    ),
    fontSize = 14.sp,
    letterSpacing = 1.sp,
    lineHeight = 16.sp
)

val AppBarTextStyle = TextStyle(
    fontFamily = FontFamily(
        Font(
            R.font.oswald
        )
    ),
    fontSize = 20.sp,
    fontWeight = FontWeight.Bold,
    textAlign = TextAlign.Center
)

val BottomSheetLabel = TextStyle(
    fontSize = 12.sp,
    fontWeight = FontWeight.Medium,
    letterSpacing = 0.sp,
)