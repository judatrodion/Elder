package com.ustudent.app.designsystem.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.ustudent.app.designsystem.theme.ButtonTextStyle
import com.ustudent.app.designsystem.theme.ElderTheme
import com.ustudent.designsystem.R

private val BUTTON_PADDING_VALUES = PaddingValues(horizontal = 16.dp, vertical = 12.dp)

@Composable
fun ElderTextButton(
    onClick: () -> Unit,
    text: String,
    modifier: Modifier = Modifier,
    colors: ButtonColors = ButtonDefaults.textButtonColors(),
) {
    TextButton(
        onClick = onClick,
        shape = ElderTheme.buttonShape,
        colors = colors,
        modifier = modifier,
        contentPadding = BUTTON_PADDING_VALUES
    ) {
        CompositionLocalProvider(LocalTextStyle provides ButtonTextStyle) {
            Text(
                text = text, fontSize = 16.sp
            )
        }
    }
}

@Composable
fun ElderTextButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    enabled: Boolean = true,
    colors: ButtonColors = ButtonDefaults.textButtonColors(),
    content: @Composable RowScope.() -> Unit
) {
    TextButton(
        onClick = onClick,
        shape = ElderTheme.buttonShape,
        colors = colors,
        enabled = enabled,
        modifier = modifier,
        contentPadding = BUTTON_PADDING_VALUES
    ) {
        CompositionLocalProvider(LocalTextStyle provides ButtonTextStyle) {
            content()
        }
    }
}

@Composable
fun ElderButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        backgroundColor = colorResource(id = R.color.green_button_color),
        contentColor = MaterialTheme.colors.onPrimary
    ),
    content: @Composable RowScope.() -> Unit
) {
    Button(
        onClick = onClick,
        colors = colors,
        modifier = modifier.height(40.dp),
        contentPadding = PaddingValues(horizontal = 16.dp),
        shape = ElderTheme.buttonShape,
        elevation = ButtonDefaults.elevation(
            defaultElevation = 0.dp,
            pressedElevation = 0.dp,
            disabledElevation = 0.dp,
            hoveredElevation = 0.dp,
            focusedElevation = 0.dp
        )
    ) {
        CompositionLocalProvider(LocalTextStyle provides ButtonTextStyle) {
            content()
        }
    }
}

@Composable
fun UStudentOutlinedButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        backgroundColor = MaterialTheme.colors.surface,
        contentColor = colorResource(id = R.color.grey_content_outlined_button)
    ),
    content: @Composable RowScope.() -> Unit
) {
    OutlinedButton(
        onClick = onClick,
        modifier = modifier,
        enabled = enabled,
        colors = colors,
        shape = ElderTheme.buttonShape,
        border = BorderStroke(1.dp, colorResource(id = R.color.border_stroke)),
        contentPadding = PaddingValues(horizontal = 12.dp, vertical = 12.dp),
        content = content
    )
}


@Composable
fun ElderButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        backgroundColor = colorResource(id = R.color.green_button_color),
        contentColor = MaterialTheme.colors.onPrimary
    ),
    isLoading: Boolean = false,
    content: @Composable RowScope.() -> Unit
) {
    ElderButton(
        onClick = {
            if (!isLoading) {
                onClick()
            }
        }, modifier = modifier, colors = colors
    ) {
        if (isLoading) {
            CircularProgressIndicator(
                color = MaterialTheme.colors.onPrimary,
                strokeWidth = 2.dp,
                modifier = Modifier.size(18.dp)
            )
        } else {
            content()
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SurfaceButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    borderStroke: BorderStroke? = BorderStroke(
        1.dp, colorResource(id = R.color.border_stroke)
    ),
    shape: Shape = RoundedCornerShape(4.dp),
    color: Color = MaterialTheme.colors.surface,
    contentColor: Color = MaterialTheme.colors.onSurface,
    elevation: Dp = 1.dp,
    contentPadding: PaddingValues = PaddingValues(
        start = 16.dp, end = 16.dp, top = 16.dp, bottom = 16.dp
    ),
    content: @Composable RowScope.() -> Unit
) {
    Surface(
        onClick = onClick,
        modifier = modifier,
        border = borderStroke,
        shape = shape,
        color = color,
        contentColor = contentColor,
        elevation = elevation
    ) {
        CompositionLocalProvider(LocalTextStyle provides MaterialTheme.typography.caption) {
            Row(
                modifier = Modifier.padding(contentPadding),
                verticalAlignment = Alignment.CenterVertically
            ) {
                content()
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ElderButtonPreview() {
    ElderTheme {
        ElderButton(
            onClick = { /*TODO*/ }, modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 16.dp)
        ) {
            Text(text = "Продолжить")
        }
    }
}
