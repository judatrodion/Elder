package com.ustudent.app.designsystem.theme

import androidx.compose.ui.graphics.Color

val md_theme_light_primary = Color(0xFF2A923F)
val md_theme_light_onPrimary = Color(0xFFF1EFEF)
val md_theme_light_secondary = Color(0xFF89E589)
val md_theme_light_onSecondary = Color(0xFF0D1F08)
val md_theme_light_background = Color(0xFFF4F8F9)
val md_theme_light_onBackground = Color(0xFF1c1e1b)
val md_theme_light_surface = Color(0xFFFDFFFF)
val md_theme_light_onSurface = Color(0xFF1A1C19)

val md_theme_dark_primary = Color(0xFF2A923F)
val md_theme_dark_onPrimary = Color(0xFFF1EFEF)
val md_theme_dark_secondary = Color(0xFF89E589)
val md_theme_dark_onSecondary = Color(0xFF0D1F08)
val md_theme_dark_background = Color(0xFF1A1C19)
val md_theme_dark_onBackground = Color(0xFFE2E3DD)
val md_theme_dark_surface = Color(0xFF272726)
val md_theme_dark_onSurface = Color(0xFFE2E3DD)

val md_theme_light_error = Color(0xFFD05D09)
val md_theme_light_onError = Color(0xFFFFFFFF)

val md_theme_dark_error = Color(0xFFFF9590)
val md_theme_dark_onError = Color(0xFF601410)