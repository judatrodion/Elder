package com.ustudent.app.designsystem.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import com.ustudent.designsystem.R

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun UStudentIconButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    color: Color = colorResource(id = R.color.green_button_color),
    contentColor: Color = MaterialTheme.colors.onPrimary,
    borderColor: Color = colorResource(id = R.color.green_button_color),
    content: @Composable () -> Unit
) {
    CompositionLocalProvider(LocalContentAlpha provides 1f) {
        Surface(
            onClick = onClick,
            shape = RoundedCornerShape(8.dp),
            color = color,
            contentColor = contentColor,
            border = BorderStroke(1.dp, borderColor),
            modifier = modifier.defaultMinSize(48.dp, 48.dp)
        ) {
            Box(contentAlignment = Alignment.Center) {
                content()
            }
        }
    }
}