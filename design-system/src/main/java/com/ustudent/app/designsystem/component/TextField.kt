package com.ustudent.app.designsystem.component

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ustudent.app.designsystem.theme.ElderTheme
import com.ustudent.designsystem.R


@Composable
fun ElderTextField(
    value: String,
    onValueChange: (String) -> Unit,
    hint: String = "",
    modifier: Modifier = Modifier,
    isError: Boolean = false,
    errorText: String? = null,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    singleLine: Boolean = false,
    maxLines: Int = Int.MAX_VALUE,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    onTextLayout: (TextLayoutResult) -> Unit = {},
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    cursorBrush: Brush = SolidColor(MaterialTheme.colors.onSurface),
) {
    BasicTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier,
        enabled = enabled,
        readOnly = readOnly,
        textStyle = textStyle.copy(color = MaterialTheme.colors.onSurface),
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        singleLine = singleLine,
        maxLines = maxLines,
        visualTransformation = visualTransformation,
        onTextLayout = onTextLayout,
        interactionSource = interactionSource,
        cursorBrush = cursorBrush
    ) { content ->
        val color = if (isError) {
            MaterialTheme.colors.error
        } else {
            colorResource(id = R.color.border_stroke)
        }
        val borderColor by animateColorAsState(targetValue = color)
        val border = BorderStroke(1.dp, borderColor)
        val elevation by animateDpAsState(targetValue = if (isError) 5.dp else 0.dp)
        val textFieldBackground =
            if (enabled) colorResource(id = R.color.grey_text_field_background) else colorResource(id = R.color.grey_text_field_not_enabled_background)
        val errorPaddingValues = if (isError) PaddingValues(
            bottom = 6.dp, start = 0.dp, end = 0.dp
        ) else PaddingValues(0.dp)
        Column(Modifier.animateContentSize()) {
            Surface(
                shape = RoundedCornerShape(15.dp),
                color = textFieldBackground,
                border = border,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(errorPaddingValues)
                    .shadow(
                        elevation,
                        shape = RoundedCornerShape(15.dp),
                        clip = true,
                        ambientColor = MaterialTheme.colors.error,
                        spotColor = MaterialTheme.colors.error
                    )
            ) {
                Box(modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth()) {
                    if (value.isEmpty()) {
                        val textColor =
                            if (isError) colorResource(id = R.color.orange_error_text) else colorResource(
                                id = R.color.grey_text_field_foreground
                            )
                        Text(
                            text = hint,
                            color = animateColorAsState(targetValue = textColor).value,
                            style = textStyle
                        )
                    }
                    content.invoke()
                }
            }
            if (isError && errorText != null) {
                Row(modifier = Modifier.padding(start = 6.dp)) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_error_alert),
                        contentDescription = null,
                        modifier = Modifier.size(12.dp),
                        tint = MaterialTheme.colors.error
                    )
                    Text(
                        text = errorText,
                        style = ElderTheme.errorTextStyle,
                        color = MaterialTheme.colors.error,
                        modifier = Modifier.padding(start = 4.dp)
                    )
                }
            }
        }
    }
}

@Preview
@Composable
fun ElderTextFieldErrorPreview() {
    ElderTheme {
        Surface {
            ElderTextField(
                value = "",
                onValueChange = {},
                hint = "Hint",
                isError = true,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ElderTextFieldPreview() {
    ElderTheme {
        ElderTextField(
            value = "",
            onValueChange = {},
            hint = "Hint",
            isError = false,
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun NotEnabledTextFieldPreview() {
    ElderTheme {
        ElderTextField(
            value = "",
            onValueChange = {},
            hint = "Hint",
            isError = false,
            enabled = false,
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        )
    }
}