package com.ustudent.app.designsystem.icon

import androidx.annotation.DrawableRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import com.ustudent.designsystem.R

object UstudentIcons {
    val Done = R.drawable.ic_done
    val Add = R.drawable.ic_add
    val BottomSheetLine = R.drawable.ic_bottom_sheet
    val Calendar = R.drawable.ic_calendar
    val CheckAll = R.drawable.ic_check_all
    val CheckBox = R.drawable.ic_check_box
    val Date = R.drawable.ic_date
    val Delete = R.drawable.ic_delete
    val Edit = R.drawable.ic_edit
    val ErrorAlert = R.drawable.ic_error_alert
    val Events = R.drawable.ic_events
    val Group = R.drawable.ic_group
    val History = R.drawable.ic_history
    val HomeWork = R.drawable.ic_home_work
    val Like = R.drawable.ic_like
    val NavigateRight = R.drawable.ic_navigate_right
    val NavigateUp = R.drawable.ic_navigate_up
    val Notification = R.drawable.ic_notification
    val Person = R.drawable.ic_person
    val Question = R.drawable.ic_question
    val QuoteClose = R.drawable.ic_quote_close
    val QuoteOpen = R.drawable.ic_quote_open
    val Sad = R.drawable.ic_sad
    val Save = R.drawable.ic_save
    val Schedule = R.drawable.ic_schedule
    val Search = R.drawable.ic_search
    val Send = R.drawable.ic_send
    val Settings = R.drawable.ic_settings
    val Share = R.drawable.ic_share
    val Stats = R.drawable.ic_stats
    val Subject = R.drawable.ic_subject
    val Time = R.drawable.ic_time
    val Home = R.drawable.ic_home
    val CheckboxNoPadding = R.drawable.ic_checkbox_no_padding
}

sealed class Icon {
    @Composable
    abstract fun getIcon(): ImageVector
    data class ImageVectorIcon(val imageVector: ImageVector) : Icon() {
        @Composable
        override fun getIcon() = imageVector
    }

    data class DrawableResourceIcon(@DrawableRes val id: Int) : Icon() {
        @Composable
        override fun getIcon() = ImageVector.vectorResource(id = id)
    }
}