package com.ustudent.app.ustudent

import com.app.ustudent.domain.model.Lessons
import com.app.ustudent.domain.model.getCurrentLesson
import org.junit.Test
import java.util.*

class LessonTest {

    @Test
    fun beforeFirstLessonStarted() {
        val lessonTime = getLessonTime(5, 30)
        assert(getCurrentLesson(lessonTime) == Lessons.FIRST)
    }

    @Test
    fun duringFirstLesson() {
        val lessonTime = getLessonTime(8, 30)
        assert(getCurrentLesson(lessonTime) == Lessons.FIRST)
    }

    @Test
    fun beforeSecondLessonStarted() {
        val lessonTime = getLessonTime(9, 34)
        assert(getCurrentLesson(lessonTime) == Lessons.FIRST)
    }

    @Test
    fun duringSecondLesson() {
        val lessonTime = getLessonTime(10, 30)
        assert(getCurrentLesson(lessonTime) == Lessons.SECOND)
    }

    @Test
    fun beforeThirdLessonStarted() {
        val lessonTime = getLessonTime(11, 20)
        assert(getCurrentLesson(lessonTime) == Lessons.SECOND)
    }

    @Test
    fun duringThirdLesson() {
        val lessonTime = getLessonTime(12, 0)
        val currentLesson = getCurrentLesson(lessonTime)
        assert(getCurrentLesson(lessonTime) == Lessons.THIRD)
    }

    @Test
    fun beforeFourthLessonStarted() {
        val lessonTime = getLessonTime(13, 30)
        val currentLesson = getCurrentLesson(lessonTime)
        assert(currentLesson == Lessons.THIRD)
    }

    @Test
    fun duringFourthLesson() {
        val lessonTime = getLessonTime(14, 0)
        val currentLesson = getCurrentLesson(lessonTime)
        assert(currentLesson == Lessons.FOURTH)
    }

    @Test
    fun beforeFifthLessonStarted() {
        val lessonTime = getLessonTime(15, 30)
        val currentLesson = getCurrentLesson(lessonTime)
        assert(currentLesson == Lessons.FOURTH)
    }

    @Test
    fun duringFifthLesson() {
        val lessonTime = getLessonTime(16, 30)
        val currentLesson = getCurrentLesson(lessonTime)
        assert(currentLesson == Lessons.FIFTH)
    }

    @Test
    fun beforeSixthLessonStarted() {
        val lessonTime = getLessonTime(17, 10)
        val currentLesson = getCurrentLesson(lessonTime)
        assert(currentLesson == Lessons.FIFTH)
    }

    @Test
    fun duringSixthLesson() {
        val lessonTime = getLessonTime(18, 30)
        val currentLesson = getCurrentLesson(lessonTime)
        assert(currentLesson == Lessons.SIXTH)
    }

    @Test
    fun afterSixthLesson() {
        val lessonTime = getLessonTime(20, 30)
        val currentLesson = getCurrentLesson(lessonTime)
        assert(currentLesson == Lessons.SIXTH)
    }

    private fun getLessonTime(hour: Int, minute: Int): Calendar {
        val c = Calendar.getInstance()
        return c.apply { this.set(Calendar.HOUR_OF_DAY, hour); this.set(Calendar.MINUTE, minute) }
    }
}