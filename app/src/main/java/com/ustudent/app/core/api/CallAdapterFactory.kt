package com.ustudent.app.core.api

import com.ustudent.app.core.functional.Either
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class CallAdapterFactory : CallAdapter.Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {

        if (returnType !is ParameterizedType) return null
        val responseType = getParameterUpperBound(0, returnType)
        if (responseType !is ParameterizedType) return null
        if (getRawType(responseType) != Either::class.java) {
            return null
        }
        val successBodyType = getParameterUpperBound(1, responseType)
        val errorBodyType = getParameterUpperBound(0, responseType)
        if (errorBodyType != Failure::class.java) {
            return null
        }
        val errorBodyConverter =
            retrofit.nextResponseBodyConverter<Result>(null, Result::class.java, annotations)

        return NetworkResponseAdapter<Any>(successBodyType, errorBodyConverter)
    }
}