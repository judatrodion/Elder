package com.ustudent.app.core.api

import android.util.Log
import com.ustudent.app.core.functional.Either
import okhttp3.Request
import okhttp3.ResponseBody
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.Response

internal class NetworkResponseCall<S : Any>(
    private val delegate: Call<S>,
    private val errorConverter: Converter<ResponseBody, Result>
) : Call<Either<Failure, S>> {

    override fun enqueue(callback: Callback<Either<Failure, S>>) {
        return delegate.enqueue(object : Callback<S> {
            override fun onResponse(call: Call<S>, response: Response<S>) {
                val body = response.body()
                val error = response.errorBody()

                if (response.isSuccessful) {
                    if (body != null) {
                        callback.onResponse(
                            this@NetworkResponseCall,
                            Response.success(Either.Right(body))
                        )
                    } else {
                        when (response.code()) {
                            204 -> callback.onResponse(
                                this@NetworkResponseCall,
                                Response.success(Either.Left(ElderApiException.NoContentError()))
                            )
                            else -> callback.onResponse(
                                this@NetworkResponseCall,
                                Response.success(Either.Left(ElderApiException.UnknownError()))
                            )
                        }
                    }
                } else {
                    val errorBody = when {
                        error == null -> null
                        error.contentLength() == 0L -> null
                        else -> try {
                            errorConverter.convert(error)
                        } catch (ex: Exception) {
                            null
                        }
                    }
                    if (errorBody != null) {
                        val errorMessage = errorBody.message
                        callback.onResponse(
                            this@NetworkResponseCall,
                            Response.success(
                                Either.Left(
                                    ElderApiException.getFailure(errorMessage)
                                )
                            )
                        )
                    } else {
                        callback.onResponse(
                            this@NetworkResponseCall,
                            Response.success(
                                Either.Left(
                                    ElderApiException.UnknownError()
                                )
                            )
                        )
                    }
                }
//                }
            }

            override fun onFailure(call: Call<S>, throwable: Throwable) {
                Log.e("NetworkResponseCall", "error  ${throwable.message}")
                val leftFailure = ElderApiException.UnknownError(throwable.message)
                callback.onResponse(
                    this@NetworkResponseCall,
                    Response.success(Either.Left(leftFailure))
                )
            }
        })
    }

    override fun isExecuted() = delegate.isExecuted

    override fun clone() = NetworkResponseCall(delegate.clone(), errorConverter)

    override fun isCanceled() = delegate.isCanceled

    override fun cancel() = delegate.cancel()

    override fun execute(): Response<Either<Failure, S>> {
        throw UnsupportedOperationException("NetworkResponseCall doesn't support execute")
    }

    override fun request(): Request = delegate.request()

    override fun timeout(): Timeout = delegate.timeout()
}