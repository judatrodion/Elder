package com.ustudent.app.core.converters

import com.squareup.moshi.Types
import com.ustudent.app.core.api.BaseResponse
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

class ResponseConverterFactory : Converter.Factory() {

    override fun responseBodyConverter(
        type: Type, annotations: Array<out Annotation>, retrofit: Retrofit
    ): Converter<ResponseBody, *> {
        val responseType: Type = Types.newParameterizedType(BaseResponse::class.java, type) as Type
        val delegate =
            retrofit.nextResponseBodyConverter<BaseResponse<*>>(this, responseType, annotations)
        return Converter<ResponseBody, Any> {
            val response = delegate.convert(it)
            return@Converter response?.result ?: response?.result
        }
    }
}