package com.ustudent.app.core.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BaseResponse<out T>(
    @Json(name = "result") val result: T?,
    @Json(name = "success") val success: Boolean
)

@JsonClass(generateAdapter = true)
data class Result(
    @Json(name = "message") val message: String,
    @Json(name = "status") val status: Boolean
)