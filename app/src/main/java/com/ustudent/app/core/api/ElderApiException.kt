package com.ustudent.app.core.api

object ElderApiException {

    fun getFailure(message: String?): Failure {
        return Error(message)
    }

    class UnknownError(msg: String? = "unknownError") : Failure.FeatureFailure()
    class NoContentError : Failure.FeatureFailure()
    class Error(val message: String?) : Failure.FeatureFailure()
    class AuthError(val message: String?) : Failure.FeatureFailure()
}