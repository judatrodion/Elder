package com.ustudent.app.core.api

import com.ustudent.app.core.functional.Either
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Converter
import java.lang.reflect.Type

class NetworkResponseAdapter<S : Any>(
    private val successType: Type,
    private val errorBodyConverter: Converter<ResponseBody, Result>
) : CallAdapter<S, Call<Either<Failure, S>>> {

    override fun responseType(): Type = successType

    override fun adapt(call: Call<S>): Call<Either<Failure, S>> {
        return NetworkResponseCall(call, errorBodyConverter)
    }
}