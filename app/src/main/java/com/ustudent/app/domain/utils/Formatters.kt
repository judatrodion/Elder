package com.ustudent.app.domain.utils

fun formatGroupName(groupName: String): String {
    return groupName.uppercase().replace(Regex("[^А-Яа-я0-9]"), "")
}