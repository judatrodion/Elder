package com.ustudent.app.domain.repository

interface AppPreferences {
    fun saveGroupName(name: String)
    fun getGroupName(): String
}