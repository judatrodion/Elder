package com.ustudent.app.domain.repository

import com.ustudent.app.domain.model.group.Group
import com.ustudent.app.domain.model.student.Student

interface GroupRepo {
    suspend fun parseGroup(groupNumber: String): List<Student>
    suspend fun saveGroup(group: Group)
    suspend fun getGroup(): Group?
}