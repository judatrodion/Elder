package com.ustudent.app.domain.usecase.feature.group

import com.ustudent.app.domain.model.group.Group
import com.ustudent.app.domain.repository.GroupRepo
import com.ustudent.app.domain.usecase.base.UseCase
import javax.inject.Inject

class SaveGroupUseCase @Inject constructor(
    private val groupRepo: GroupRepo
) : UseCase<Unit, SaveGroupUseCase.Params>() {
    override suspend fun buildUseCase(params: Params) {
        groupRepo.saveGroup(params.group)
    }

    data class Params(
        val group: Group
    ) {
        companion object {
            fun group(group: Group) = Params(
                group
            )
        }
    }
}