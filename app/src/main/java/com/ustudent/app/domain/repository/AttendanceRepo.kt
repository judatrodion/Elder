package com.ustudent.app.domain.repository

import com.ustudent.app.domain.model.attendance.AttendanceReport

interface AttendanceRepo {
    fun saveReport(report: AttendanceReport)
}