package com.ustudent.app.domain.model.attendance

import com.ustudent.app.domain.model.student.Student
import com.ustudent.app.ui.module.home.screen.attendance.model.AttendanceFilter
import kotlinx.datetime.LocalDate
import java.time.format.TextStyle
import java.util.*

data class AttendanceReport(
    val groupName: String,
    val filter: AttendanceFilter,
    val lesson: ReportLessonModel,
    val students: List<AttendanceStudentModel>,
) {
    fun getSubject(): String {
        return "$groupName, ${lesson.getFormattedDate()}. ${lesson.lessonName}"
    }

    fun getBody(): String {
        val subject = getSubject()
        val body = StringBuilder().apply {
            appendLine(subject)
            if (filter == AttendanceFilter.ALL || filter == AttendanceFilter.ATTEMPTING) {
                val attemptingStudents = students.filter { it.isAttending }
                appendLine("Присутствующие: ${attemptingStudents.size}/${students.size}")
                append(
                    attemptingStudents.joinToString(separator = "\n") {
                        it.getName()
                    }
                )
                appendLine()
            }
            appendLine()
            if (filter == AttendanceFilter.ALL || filter == AttendanceFilter.MISSING) {
                val missingStudents = students.filter { !it.isAttending }
                appendLine("Отсутствующие: ${missingStudents.size}/${students.size}")
                append(
                    missingStudents.joinToString(separator = "\n") {
                        it.getName() + if (it.hasReason) " ${it.getReason()}" else ""
                    }
                )
            }
        }
        return body.toString()
    }
}

data class AttendanceStudentModel(
    val firstName: String,
    val lastName: String,
    val middleName: String?,
    val isAttending: Boolean,
    val hasReason: Boolean,
    val reasonOfMissing: String
) {
    fun getName(): String {
        return "$lastName ${firstName.first()}.${middleName?.first() ?: ""}."
    }

    fun getReason(): String {
        return if (hasReason && reasonOfMissing.isNotBlank()) {
            "($reasonOfMissing)"
        } else {
            ""
        }
    }
}

fun Student.toAttendanceStudent(): AttendanceStudentModel {
    return AttendanceStudentModel(
        firstName = name,
        lastName = surname,
        middleName = patronymic,
        isAttending = false,
        hasReason = false,
        ""
    )
}


data class ReportLessonModel(
    val number: Int,
    val date: LocalDate,
    val lessonName: String
) {
    fun getFormattedDate(): String {
        return "${
            date.dayOfWeek.getDisplayName(
                TextStyle.FULL,
                Locale("ru")
            )
        }, ${
            date.dayOfMonth
        } ${
            date.month.getDisplayName(
                TextStyle.FULL,
                Locale("ru")
            )
        }"
    }
}