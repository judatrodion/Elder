package com.ustudent.app.domain.model

import java.util.*

enum class Lessons(
    val value: String
) {
    FIRST("1-ая пара"),
    SECOND("2-ая пара"),
    THIRD("3-я пара"),
    FOURTH("4-ая пара"),
    FIFTH("5-ая пара"),
    SIXTH("6-ая пара")
}

fun getCurrentLesson(c: Calendar = Calendar.getInstance()): Lessons {
    val hours = c.get(Calendar.HOUR_OF_DAY)
    val minutes = c.get(Calendar.MINUTE)
    return when (hours) {
        in 0..8 -> Lessons.FIRST
        9 -> if (minutes < 45) Lessons.FIRST else Lessons.SECOND
        10 -> Lessons.SECOND
        11 -> if (minutes < 30) Lessons.SECOND else Lessons.THIRD
        12 -> Lessons.THIRD
        13 -> if (minutes < 50) Lessons.THIRD else Lessons.FOURTH
        14 -> Lessons.FOURTH
        15 -> if (minutes < 35) Lessons.FOURTH else Lessons.FIFTH
        16 -> Lessons.FIFTH
        17 -> if (minutes < 20) Lessons.FIFTH else Lessons.SIXTH
        else -> Lessons.SIXTH
    }
}