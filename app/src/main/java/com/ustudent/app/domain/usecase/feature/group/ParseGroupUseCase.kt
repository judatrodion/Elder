package com.ustudent.app.domain.usecase.feature.group

import com.ustudent.app.domain.model.student.Student
import com.ustudent.app.domain.repository.GroupRepo
import com.ustudent.app.domain.usecase.base.UseCase
import javax.inject.Inject

class ParseGroupUseCase @Inject constructor(
    private val groupRepo: GroupRepo
) : UseCase<List<Student>, String>() {
    override suspend fun buildUseCase(params: String): List<Student> {
        return groupRepo.parseGroup(convertGroupName(params))
    }

    private fun convertGroupName(name: String): String {
        return name.trim().lowercase().replace("-", "").replace(" ", "")
    }
}