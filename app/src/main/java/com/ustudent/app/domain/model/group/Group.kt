package com.ustudent.app.domain.model.group

import com.ustudent.app.domain.model.student.Student

data class Group(
    val name: String,
    val students: List<Student>
)