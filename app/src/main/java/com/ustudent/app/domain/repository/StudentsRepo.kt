package com.ustudent.app.domain.repository

import com.ustudent.app.domain.model.student.Student

interface StudentsRepo {
    suspend fun getStudents(): List<Student>
}