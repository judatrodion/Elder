package com.ustudent.app.domain.mapper

import com.ustudent.app.data.storage.local.group.GroupObject
import com.ustudent.app.data.storage.local.students.StudentObject
import com.ustudent.app.domain.model.group.Group
import com.ustudent.app.domain.model.student.Student

fun Student.map(): StudentObject {
    return StudentObject(
        this.id,
        this.name,
        this.surname,
        this.patronymic
    )
}

fun Group.map(): GroupObject {
    return GroupObject(
        this.name,
        this.students
    )
}