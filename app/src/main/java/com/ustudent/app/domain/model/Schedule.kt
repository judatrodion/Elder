package com.ustudent.app.domain.model

import java.time.DayOfWeek
import java.util.*

data class Schedule(
    private val firstWeek: Week,
    private val secondWeek: Week
) {

    private fun getCurrentWeek(): Week {
        if (Calendar.getInstance().get(Calendar.WEEK_OF_YEAR) % 2 == 0) {
            return secondWeek
        }
        return firstWeek
    }

    fun getCurrentDayReport(day: Int = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1): Day {
        val week = getCurrentWeek()
        return when (day) {
            1 -> week.monday
            2 -> week.thursday
            3 -> week.wednesday
            4 -> week.tuesday
            5 -> week.friday
            else -> week.monday
        }
    }
}

data class Week(
    val monday: Day,
    val tuesday: Day,
    val wednesday: Day,
    val thursday: Day,
    val friday: Day,
    val saturday: Day
)

data class Day(
    val firstLesson: Lesson? = null,
    val secondLesson: Lesson? = null,
    val thirdLesson: Lesson? = null,
    val fourthLesson: Lesson? = null,
    val fifthLesson: Lesson? = null,
    val sixthLesson: Lesson? = null,
) {
    fun generateDay(): String {
        return generateLesson(firstLesson, 1) +
                generateLesson(secondLesson, 2) +
                generateLesson(thirdLesson, 3) +
                generateLesson(fourthLesson, 4) +
                generateLesson(fifthLesson, 5) +
                generateLesson(sixthLesson, 6)
    }

    private fun generateLesson(lesson: Lesson?, number: Int): String {
        return lesson?.generateLesson(number) ?: "$number. Пусто\n"
    }
}

data class Lesson(
    val name: String,
    val room: String,
    val teachers: List<String>?,
    val isLection: Boolean = false
) {
    fun generateLesson(number: Int): String {
        return "$number.\n$name\n$room\n${teachers?.joinToString(separator = ", ") ?: ""}\n"
    }
}

fun main() {
    println(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR))
}