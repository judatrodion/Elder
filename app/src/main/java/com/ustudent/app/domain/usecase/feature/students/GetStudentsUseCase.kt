package com.ustudent.app.domain.usecase.feature.students

import com.ustudent.app.domain.model.student.Student
import com.ustudent.app.domain.repository.StudentsRepo
import com.ustudent.app.domain.usecase.base.UseCase
import javax.inject.Inject

class GetStudentsUseCase @Inject constructor(
    private val studentsRepo: StudentsRepo
) : UseCase<List<Student>, UseCase.None>() {
    override suspend fun buildUseCase(params: None): List<Student> {
        return studentsRepo.getStudents()
    }

}