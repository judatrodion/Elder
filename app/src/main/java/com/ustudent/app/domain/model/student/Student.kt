package com.ustudent.app.domain.model.student

data class Student(
    val id: String,
    val name: String,
    val surname: String,
    val patronymic: String?
)