package com.ustudent.app.domain.usecase.feature.group

import com.ustudent.app.domain.repository.AppPreferences
import com.ustudent.app.domain.usecase.base.UseCase
import javax.inject.Inject

class GetGroupNameUseCase @Inject constructor(
    private val preferences: AppPreferences
) : UseCase<String, UseCase.None>() {
    override suspend fun buildUseCase(params: None): String {
        return preferences.getGroupName()
    }
}