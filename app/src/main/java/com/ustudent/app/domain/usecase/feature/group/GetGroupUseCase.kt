package com.ustudent.app.domain.usecase.feature.group

import com.ustudent.app.domain.model.group.Group
import com.ustudent.app.domain.repository.GroupRepo
import com.ustudent.app.domain.usecase.base.UseCase
import javax.inject.Inject

class GetGroupUseCase @Inject constructor(
    private val groupRepo: GroupRepo
) : UseCase<Group?, UseCase.None>() {
    override suspend fun buildUseCase(params: UseCase.None): Group? {
        return groupRepo.getGroup()
    }

}