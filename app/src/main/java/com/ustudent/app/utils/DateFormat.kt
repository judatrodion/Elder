package com.ustudent.app.utils

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.ustudent.app.R
import kotlinx.datetime.*
import kotlinx.datetime.TimeZone
import java.time.format.TextStyle
import java.util.*

@Composable
fun LocalDate.formatToText(): String {
    val today = Clock.System.todayIn(TimeZone.currentSystemDefault())
    val prefix: String = if (today.daysUntil(this) == 0) {
        stringResource(id = R.string.today)
    } else if (today.daysUntil(this) == 1) {
        stringResource(id = R.string.tomorrow)
    } else {
        this.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.getDefault())
    }
    return "${prefix.first().uppercase() + prefix.drop(1)}, ${this.dayOfMonth} ${
        this.month.getDisplayName(
            TextStyle.FULL,
            Locale.getDefault()
        ).lowercase()}"
}