package com.ustudent.app

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ustudent.app.domain.usecase.base.UseCase
import com.ustudent.app.domain.usecase.feature.group.GetGroupNameUseCase
import com.ustudent.app.ui.utils.ElderScreens
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    getGroupNameUseCase: GetGroupNameUseCase
) : ViewModel() {

    private val _screenLD = MutableStateFlow<ElderScreens>(ElderScreens.Splash)
    val screenLD = _screenLD.asStateFlow()

    init {
        getGroupNameUseCase.execute(
            params = UseCase.None(),
            scope = viewModelScope,
            onSuccess = {
                _screenLD.update { ElderScreens.Home }
            },
            onError = {
                _screenLD.update { ElderScreens.Login }
            },
        )
    }

    fun launchHome() {
        _screenLD.update { ElderScreens.Home }
    }
}