package com.ustudent.app.data.storage.local.students

import com.ustudent.app.domain.model.student.Student
import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject

class StudentObject() : RealmObject {
    var id: String = ObjectId.create().toString()
    var name: String = ""
    var surname: String = ""
    var patronymic: String? = null

    constructor(id: String, name: String, surname: String, patronymic: String?) : this() {
        this.id = id
        this.name = name
        this.surname = surname
        this.patronymic = patronymic
    }

    fun toStudent(): Student {
        return Student(id, name, surname, patronymic)
    }
}