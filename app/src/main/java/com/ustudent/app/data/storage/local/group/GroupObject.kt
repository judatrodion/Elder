package com.ustudent.app.data.storage.local.group

import com.ustudent.app.data.storage.local.students.StudentObject
import com.ustudent.app.domain.mapper.map
import com.ustudent.app.domain.model.group.Group
import com.ustudent.app.domain.model.student.Student
import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.ext.toRealmList
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject

class GroupObject() : RealmObject {
    var name: String = ""
    var students: RealmList<StudentObject> = realmListOf()

    constructor(name: String, students: List<Student>) : this() {
        this.name = name
        this.students = students.map { it.map() }.toRealmList()
    }

    fun update(newObject: GroupObject) {
        name = newObject.name
        students = newObject.students
    }

    fun toGroup(): Group {
        return Group(name, students.map { it.toStudent() })
    }
}

