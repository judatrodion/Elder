package com.ustudent.app.data.storage.api

import com.ustudent.app.data.storage.api.dto.group.GroupDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface ElderApi {

    @GET("students")
    suspend fun getGroup(@Query(value = "group") groupName: String): GroupDTO
}