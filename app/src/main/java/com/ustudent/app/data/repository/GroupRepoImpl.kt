package com.ustudent.app.data.repository

import com.ustudent.app.data.storage.api.ElderApi
import com.ustudent.app.data.storage.local.group.GroupObject
import com.ustudent.app.data.storage.local.students.StudentObject
import com.ustudent.app.domain.model.group.Group
import com.ustudent.app.domain.model.student.Student
import com.ustudent.app.domain.repository.GroupRepo
import com.ustudent.app.domain.repository.AppPreferences
import com.ustudent.app.domain.utils.formatGroupName
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.ext.query
import javax.inject.Inject

class GroupRepoImpl @Inject constructor(
    private val api: ElderApi,
    private val preferences: AppPreferences,
    private val realmConfiguration: RealmConfiguration
) : GroupRepo {
    override suspend fun parseGroup(groupNumber: String): List<Student> {
        return api.getGroup(groupNumber).toStudents()
    }

    override suspend fun saveGroup(group: Group) {
        val formattedName = formatGroupName(group.name)
        preferences.saveGroupName(formattedName)
        val realm = Realm.open(realmConfiguration)
        realm.write {
            val realmGroup = GroupObject(name = formattedName, students = group.students)
            val oldStudents = query<StudentObject>().find()
            delete(oldStudents)
            query<GroupObject>().first().find()?.update(realmGroup) ?: copyToRealm(realmGroup)
        }
        realm.close()
    }

    override suspend fun getGroup(): Group? {
        val realm = Realm.open(realmConfiguration)
        val group = realm.query<GroupObject>().first().find()?.toGroup()
        realm.close()
        return group
    }
}