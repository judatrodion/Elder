package com.ustudent.app.data.repository

import com.ustudent.app.data.storage.local.students.StudentObject
import com.ustudent.app.domain.model.student.Student
import com.ustudent.app.domain.repository.StudentsRepo
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.ext.query
import javax.inject.Inject

class StudentsRepoImpl @Inject constructor(
    private val realmConfiguration: RealmConfiguration
) : StudentsRepo {
    override suspend fun getStudents(): List<Student> {
        val realm = Realm.open(realmConfiguration)
        val students = realm.query<StudentObject>().find()
        return students.map { it.toStudent() }
    }
}