package com.ustudent.app.data.storage.preferences

data class GroupName(
    val name: String
)