package com.ustudent.app.data.repository

import com.orhanobut.hawk.Hawk
import com.ustudent.app.domain.repository.AppPreferences
import javax.inject.Inject

class PreferencesImpl @Inject constructor() : AppPreferences {
    override fun saveGroupName(name: String) {
        Hawk.put(GROUP_NAME, name)
    }

    override fun getGroupName(): String {
        return Hawk.get(GROUP_NAME)
    }

    companion object {
        private const val GROUP_NAME = "group_name"
    }
}