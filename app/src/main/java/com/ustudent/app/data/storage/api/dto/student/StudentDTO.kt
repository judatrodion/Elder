package com.ustudent.app.data.storage.api.dto.student

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StudentDTO(
    val data: String
)
