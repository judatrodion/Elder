package com.ustudent.app.data.storage.api.dto.group

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.ustudent.app.domain.model.student.Student
import io.realm.kotlin.types.ObjectId

@JsonClass(generateAdapter = true)
data class GroupDTO(
    @Json(name = "result") val result: List<String>
) {
    fun toStudents(): List<Student> {
        return result.map {
            val tokens = it.split(' ')
            val name = tokens[1]
            val surname = tokens[0]
            val patronymic = if (tokens.size == 3) tokens[2] else null
            Student(
                id = ObjectId.create().toString(),
                name = name,
                surname = surname,
                patronymic = patronymic
            )
        }
    }
}
