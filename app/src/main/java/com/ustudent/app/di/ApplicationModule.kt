package com.ustudent.app.di

import com.ustudent.app.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.ustudent.app.core.api.CallAdapterFactory
import com.ustudent.app.data.storage.api.ElderApi
import com.ustudent.app.data.storage.local.group.GroupObject
import com.ustudent.app.data.storage.local.students.StudentObject
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.realm.kotlin.RealmConfiguration
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    private val schemaVersion = 1L

    @Provides
    @Singleton
    @Named("global")
    fun provideRetrofit(
        moshiConverterFactory: MoshiConverterFactory,
        scalarsConverterFactory: ScalarsConverterFactory,
//        callAdapterFactory: CallAdapterFactory,
        @Named("global") httpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder()
        .addConverterFactory(scalarsConverterFactory)
        .addConverterFactory(moshiConverterFactory)
//        .addCallAdapterFactory(callAdapterFactory)
        .client(httpClient)
        .baseUrl(BuildConfig.BASE_URL)
        .build()

    @Provides
    @Singleton
    fun provideCallAdapterFactory(): CallAdapterFactory =
        CallAdapterFactory()

    @Provides
    @Singleton
    fun provideApiService(@Named("global") retrofit: Retrofit): ElderApi =
        retrofit.create(ElderApi::class.java)

    @Provides
    @Singleton
    fun provideInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Provides
    @Singleton
    @Named("global")
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .connectTimeout(15, TimeUnit.SECONDS)
        .build()

    @Provides
    @Singleton
    fun provideRealmConfiguration() = RealmConfiguration.Builder(
        setOf(
            GroupObject::class,
            StudentObject::class
        )
    )
        .name("UStudent.realm")
        .deleteRealmIfMigrationNeeded()
        .schemaVersion(schemaVersion)
        .build()

    @Provides
    @Singleton
    fun provideKotlinJsonAdapterFactory(): KotlinJsonAdapterFactory = KotlinJsonAdapterFactory()

    @Provides
    @Singleton
    fun provideMoshi(kotlinJsonAdapterFactory: KotlinJsonAdapterFactory): Moshi = Moshi.Builder()
        .add(kotlinJsonAdapterFactory)
        .build()

    @Provides
    @Singleton
    fun provideMoshiConverterFactory(moshi: Moshi): MoshiConverterFactory =
        MoshiConverterFactory.create(moshi)

    @Provides
    @Singleton
    fun provideScalarsConverterFactory(): ScalarsConverterFactory = ScalarsConverterFactory.create()

}
