package com.ustudent.app.di

import com.ustudent.app.data.repository.GroupRepoImpl
import com.ustudent.app.data.repository.PreferencesImpl
import com.ustudent.app.data.repository.StudentsRepoImpl
import com.ustudent.app.domain.repository.GroupRepo
import com.ustudent.app.domain.repository.AppPreferences
import com.ustudent.app.domain.repository.StudentsRepo
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class RemoteModule {

    @Binds
    abstract fun bindGroupRepo(groupRepoImpl: GroupRepoImpl): GroupRepo

    @Binds
    abstract fun bindPreferencesRepo(preferences: PreferencesImpl): AppPreferences

    @Binds
    abstract fun bindStudentsRepo(studentsRepo: StudentsRepoImpl): StudentsRepo
}