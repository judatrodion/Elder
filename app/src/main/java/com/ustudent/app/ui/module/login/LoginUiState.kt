package com.ustudent.app.ui.module.login

import com.ustudent.app.domain.model.group.Group
import com.ustudent.app.domain.model.student.Student

data class LoginUiState(
    val groupNumber: String,
    val nameIsValid: Boolean,
    val parsedSuccess: Boolean?,
    val students: List<Student>?,
    val savedSuccess: Boolean?,
    val isLoading: Boolean
) {
    fun getGroup(): Group = Group(groupNumber, students ?: emptyList())
}