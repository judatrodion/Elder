package com.ustudent.app.ui.module.home

import com.ustudent.app.R
import com.ustudent.app.ui.base.navigation.UStudentNavigationDestination

object HomeDestination : UStudentNavigationDestination {
    override val route = "home_destination"
    override val textId: Int
        get() = R.string.home_top_destination
}