package com.ustudent.app.ui.app.component

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.ustudent.app.designsystem.theme.ElderTheme

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DrawerNavigationItem(
    icon: ImageVector,
    title: String,
    count: Int?,
    active: Boolean,
    onItemClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    val color = if (active) MaterialTheme.colors.secondary else MaterialTheme.colors.background
    val contentColor =
        if (active) MaterialTheme.colors.onSecondary else MaterialTheme.colors.onBackground
    Surface(
        color = animateColorAsState(targetValue = color).value,
        contentColor = contentColor,
        modifier = modifier,
        shape = RoundedCornerShape(40.dp),
        onClick = {
            onItemClick()
        }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp, vertical = 10.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                NavItemTitle(icon, title)
            }
            if (count != 0) {
                if (!active) {
                    Box(
                        modifier = Modifier
                            .background(
                                MaterialTheme.colors.onBackground,
                                shape = CircleShape
                            )
                            .size(24.dp),
                        contentAlignment = Alignment.Center
                    ) {
                        Text(
                            text = count.toString(),
                            fontSize = 14.sp,
                            fontWeight = FontWeight.SemiBold,
                            fontFamily = FontFamily(Font(com.ustudent.designsystem.R.font.oswald)),
                            color = MaterialTheme.colors.background,
                            modifier = Modifier.fillMaxHeight()
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun NavItemTitle(
    icon: ImageVector,
    title: String
) {
    Icon(imageVector = icon, contentDescription = null, modifier = Modifier.size(24.dp))
    Spacer(Modifier.width(24.dp))
    Text(text = title, style = NavigationItemTextStyle)
}

private val NavigationItemTextStyle = TextStyle(
    fontSize = 16.sp,
    fontWeight = FontWeight.SemiBold,
    fontFamily = FontFamily(
        Font(com.ustudent.designsystem.R.font.oswald)
    ),
    letterSpacing = 0.sp
)

@Preview(name = "Active drawer item", showBackground = true)
@Composable
fun PreviewDrawerItem() {
    ElderTheme {
        DrawerNavigationItem(
            icon = Icons.Default.Person,
            title = "Title",
            count = 10,
            active = true,
            onItemClick = {},
            modifier = Modifier.padding(16.dp)
        )
    }
}

@Preview(name = "Inactive drawer item", showBackground = true)
@Composable
fun PreviewDrawerInactiveItem() {
    ElderTheme {
        DrawerNavigationItem(
            icon = Icons.Default.Person,
            title = "Title",
            count = 10,
            active = false,
            onItemClick = {},
            modifier = Modifier.padding(16.dp)
        )
    }
}