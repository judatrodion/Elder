package com.ustudent.app.ui.module.login

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import com.ustudent.app.ui.module.login.screen.ErrorDialog
import com.ustudent.app.ui.module.login.screen.InputGroupNumberScreen

@Composable
fun LoginModule(viewModel: LoginViewModel = hiltViewModel(), onNavigateHome: () -> Unit) {
    val loginUiState by viewModel.loginState.collectAsState()
    if (loginUiState.savedSuccess == true) {
        onNavigateHome()
        viewModel.resetGroupSavedState()
    }
    InputGroupNumberScreen(
        groupName = loginUiState.groupNumber,
        onGroupNameChange = viewModel::onGroupNameChange,
        isGroupNameValid = loginUiState.nameIsValid,
        onContinueClicked = {
            viewModel.onContinueClicked()
        },
        isLoading = loginUiState.isLoading
    )
    ErrorDialog(
        onTryAgainClicked = {
            viewModel.onCloseDialog()
        },
        visible = loginUiState.parsedSuccess == false
    )
}