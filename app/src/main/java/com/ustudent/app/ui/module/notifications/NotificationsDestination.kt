package com.ustudent.app.ui.module.notifications

import com.ustudent.app.R
import com.ustudent.app.ui.base.navigation.UStudentNavigationDestination

object NotificationsDestination : UStudentNavigationDestination {
    override val route: String
        get() = "notifications_destination"
    override val textId: Int
        get() = R.string.notifications
}