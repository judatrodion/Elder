package com.ustudent.app.ui.module.home.screen.attendance.model

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.ustudent.app.R

data class LessonAttendanceUiState(
    val number: Int,
    val lessonName: String
) {
    @Composable
    fun toText(): String {
        return "$number ${stringResource(id = R.string.lesson).lowercase()} | $lessonName"
    }
}
