package com.ustudent.app.ui.module.home.screen.attendance.component

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.ustudent.app.R
import com.ustudent.app.designsystem.component.ElderTextButton
import com.ustudent.app.designsystem.component.ElderTextField
import com.ustudent.app.designsystem.icon.UstudentIcons
import com.ustudent.app.designsystem.theme.ButtonTextStyle
import com.ustudent.app.domain.model.attendance.AttendanceStudentModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(
    ExperimentalMaterialApi::class, ExperimentalAnimationApi::class
)
@Composable
fun StudentItem(
    student: AttendanceStudentModel,
    index: Int,
    onItemClick: (index: Int) -> Unit,
    onReasonEdit: (index: Int, reason: String) -> Unit,
    onClearFocus: () -> Unit,
    onAddReason: (index: Int) -> Unit,
    onDeleteReason: (index: Int) -> Unit,
    onKeyboardHide: () -> Unit,
    modifier: Modifier = Modifier,
    onScrollToItem: () -> Unit = {},
) {
    val isAttending = student.isAttending
    val reasonFieldVisible = student.hasReason
    val focusRequester = remember { FocusRequester() }
    Surface(
        onClick = {
            onItemClick(index)
            onClearFocus()
        }, modifier = modifier
            .animateContentSize()
    ) {
        Column(Modifier.padding(horizontal = 12.dp, vertical = 6.dp)) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(38.dp)
                    .padding(start = 0.dp, top = 6.dp, end = 0.dp, bottom = 6.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                StudentName(student, index + 1)
                Text(
                    text = if (isAttending) stringResource(id = R.string.one_attendances) else stringResource(
                        id = R.string.one_misses
                    ),
                    style = ButtonTextStyle,
                    color = if (isAttending) MaterialTheme.colors.primary else Color.Gray,
                    modifier = Modifier.padding(end = 16.dp),
                    fontWeight = FontWeight(490)
                )
            }
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                val scope = rememberCoroutineScope()
                AnimatedContent(targetState = reasonFieldVisible) { visible: Boolean ->
                    if (!visible) {
                        ElderTextButton(
                            onClick = {
                                onScrollToItem()
                                onAddReason(index)
                                scope.launch {
                                    delay(300)
                                    focusRequester.requestFocus()
                                }
                            },
                            enabled = !student.isAttending,
                            modifier = Modifier.padding(vertical = 3.dp)
                        ) {
                            Icon(
                                painter = painterResource(id = UstudentIcons.Add),
                                contentDescription = null
                            )
                            Text(text = "Указать причину")
                        }
                    } else {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            ElderTextField(
                                value = student.reasonOfMissing,
                                onValueChange = {
                                    onReasonEdit(index, it)
                                },
                                hint = stringResource(id = R.string.reason),
                                modifier = Modifier
                                    .weight(1f, true)
                                    .focusRequester(focusRequester),
                                enabled = !isAttending,
                                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                                keyboardActions = KeyboardActions(
                                    onDone = {
                                        onKeyboardHide()
                                        onClearFocus()
                                    }
                                ),
                            )
                            IconButton(
                                onClick = {
                                    onReasonEdit(index, "Болеет")
                                    onKeyboardHide()
                                    onClearFocus()
                                }
                            ) {
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_ill),
                                    null,
                                    tint = colorResource(id = com.ustudent.designsystem.R.color.grey_content_outlined_button)
                                )
                            }
                            IconButton(
                                onClick = { onDeleteReason(index) }
                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Cancel,
                                    contentDescription = null,
                                    tint = colorResource(id = com.ustudent.designsystem.R.color.grey_content_outlined_button)
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun StudentName(student: AttendanceStudentModel, number: Int) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        AnimatedVisibility(visible = student.isAttending) {
            Icon(
                painter = painterResource(UstudentIcons.CheckboxNoPadding),
                contentDescription = null,
                tint = MaterialTheme.colors.primary
            )
        }
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = "${number}. ${getFullName(student)}",
            fontSize = 16.sp,
            color = MaterialTheme.colors.onSurface,
            fontWeight = FontWeight(490),
            letterSpacing = 0.1f.sp,
            lineHeight = 24.sp
        )
    }
}

private fun getFullName(student: AttendanceStudentModel): String {
    val patronymic = if (student.middleName != null) "${student.middleName.first()}." else ""
    return "${student.lastName} ${student.firstName.first()}.$patronymic"
}