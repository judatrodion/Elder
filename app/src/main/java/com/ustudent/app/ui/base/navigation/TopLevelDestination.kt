package com.ustudent.app.ui.base.navigation

import com.ustudent.app.designsystem.icon.Icon


data class TopLevelDestination(
    override val route: String,
    val selectedIcon: Icon,
    val unselectedIcon: Icon,
    override val textId: Int
) : UStudentNavigationDestination