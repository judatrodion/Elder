package com.ustudent.app.ui.utils

import androidx.annotation.StringRes
import com.ustudent.app.R

sealed class ElderScreens(val route: String, @StringRes val title: Int) {
    object Login : ElderScreens("LoginScreen", R.string.login)
    object Home : ElderScreens("HomeScreen", R.string.home)
    object Splash : ElderScreens("Splash", R.string.splash)
}