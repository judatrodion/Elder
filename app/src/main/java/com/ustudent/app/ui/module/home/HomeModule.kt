package com.ustudent.app.ui.module.home

import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.ustudent.app.ui.module.home.component.HomeBottomBar
import com.ustudent.app.ui.module.home.destination.homeModuleDestinations
import com.ustudent.app.ui.module.home.screen.attendance.AttendanceDestination
import com.ustudent.app.ui.module.home.screen.attendance.AttendanceScreen
import com.ustudent.app.ui.module.home.screen.events.EventsDestination
import com.ustudent.app.ui.module.home.screen.hometask.HomeTaskDestination
import com.ustudent.app.ui.module.home.screen.schedule.ScheduleDestination
import java.text.DateFormat
import java.util.*

@Composable
fun HomeModule(viewModel: HomeViewModel = hiltViewModel(), onNavigationIconClick: () -> Unit) {
    val navController = rememberNavController()
    val currentDestination = navController.currentBackStackEntryAsState().value?.destination
    var showBottomBar by rememberSaveable {
        mutableStateOf(true)
    }
    Scaffold(
        bottomBar = {
            if (showBottomBar) {
                HomeBottomBar(
                    topDestinations = homeModuleDestinations,
                    currentDestination = currentDestination,
                    onTabSelected = { destination ->
                        if (currentDestination?.route == destination.route) return@HomeBottomBar
                        navController.navigate(destination.route) {
                            // Pop up to the start destination of the graph to
                            // avoid building up a large stack of destinations
                            // on the back stack as users select items
                            popUpTo(navController.graph.findStartDestination().id) {
                                inclusive = true
                            }
                            // Avoid multiple copies of the same destination when
                            // reselecting the same item
                            launchSingleTop = true
                            // Restore state when reselecting a previously selected item
                            restoreState = true
                        }
                    }
                )
            }
        }
    ) {
        Surface(
            modifier = Modifier.padding(it), color = MaterialTheme.colors.background
        ) {
            NavHost(navController = navController, startDestination = ScheduleDestination.route) {
                composable(AttendanceDestination.route) {
                    AttendanceScreen(
                        onNavigationIconClick = {
                            onNavigationIconClick()
                        }, onBottomNavigationVisibilityChange = { visible ->
                            showBottomBar = visible
                        }
                    )
                }
                composable(ScheduleDestination.route) {
                    Text(
                        "${
                            DateFormat.getInstance().format(Calendar.getInstance().time)
                        }\n" + viewModel.schedule.getCurrentDayReport().generateDay()
                    )
                }
                composable(HomeTaskDestination.route) {
                    Text(HomeTaskDestination.route)
                }
                composable(EventsDestination.route) {
                    Text(text = EventsDestination.route)
                }
            }
        }
    }
}