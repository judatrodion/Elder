package com.ustudent.app.ui.module.login.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ustudent.app.designsystem.component.ElderButton
import com.ustudent.app.designsystem.component.ElderTextField
import com.ustudent.app.designsystem.theme.ElderTheme
import com.ustudent.app.R

@Composable
fun InputGroupNumberScreen(
    groupName: String,
    onGroupNameChange: (String) -> Unit,
    onContinueClicked: () -> Unit,
    isGroupNameValid: Boolean = true,
    isLoading: Boolean = false,
) {
    Surface(
        modifier = Modifier
            .fillMaxSize()
            .imePadding(),
        color = MaterialTheme.colors.background
    ) {
        val horizontal = 8.dp
        val vertical = 16.dp
        Box(
            contentAlignment = Alignment.BottomCenter,
            modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp)
        ) {
            ElderButton(
                onClick = { onContinueClicked() },
                modifier = Modifier.fillMaxWidth(),
                isLoading = isLoading
            ) {
                Text(text = stringResource(id = R.string._continue))
            }
        }
        Column(
            modifier = Modifier.padding(horizontal, vertical),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_elder),
                contentDescription = null,
                contentScale = ContentScale.Crop
            )
            Text(
                text = stringResource(id = R.string.your_group),
                modifier = Modifier.padding(10.dp),
                style = MaterialTheme.typography.h1
            )
            ElderTextField(
                value = groupName,
                onValueChange = onGroupNameChange,
                hint = stringResource(id = R.string.input_number),
                singleLine = true,
                maxLines = 1,
                isError = !isGroupNameValid,
                errorText = stringResource(id = R.string.empty_field),
                modifier = Modifier.width(220.dp)
            )
        }
    }
}

@Preview
@Composable
private fun ScreenPreview() {
    ElderTheme {
        InputGroupNumberScreen(
            groupName = "ПИ2002",
            onGroupNameChange = {},
            onContinueClicked = { /*TODO*/ })
    }
}