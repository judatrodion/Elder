package com.ustudent.app.ui.module.home.screen.schedule

import com.ustudent.app.R
import com.ustudent.app.ui.base.navigation.UStudentNavigationDestination

object ScheduleDestination : UStudentNavigationDestination {
    override val route: String
        get() = "schedule_destination"
    override val textId: Int
        get() = R.string.schedule
}