package com.ustudent.app.ui.module.login.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.ustudent.app.R
import com.ustudent.app.designsystem.component.ElderButton
import com.ustudent.app.designsystem.theme.ElderTheme

@Composable
fun ErrorDialog(
    onTryAgainClicked: () -> Unit,
    visible: Boolean
) {
    if (visible) {
        Dialog(
            onDismissRequest = {
                onTryAgainClicked()
            },
        ) {
            Surface(
                modifier = Modifier
                    .fillMaxWidth(),
                color = MaterialTheme.colors.background,
                shape = MaterialTheme.shapes.large
            ) {
                val horizontal = 24.dp
                val vertical = 20.dp
                Column(
                    modifier = Modifier.padding(horizontal, vertical),
                    verticalArrangement = Arrangement.Center
                ) {

                    Image(
                        imageVector = ImageVector.vectorResource(id = R.drawable.ic_elder),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                        modifier = Modifier.align(Alignment.CenterHorizontally)
                    )
                    Spacer(Modifier.height(6.dp))
                    Text(
                        text = stringResource(id = R.string.something_went_wrong),
                        textAlign = TextAlign.Center,
                        color = MaterialTheme.colors.onSurface,
                        style = MaterialTheme.typography.h1,
                        modifier = Modifier
                            .align(Alignment.CenterHorizontally)
                    )
                    Text(
                        text = stringResource(id = R.string.possible_errors),
                        modifier = Modifier.padding(
                            top = dimensionResource(
                                id = R.dimen._12dp
                            )
                        ),
                        style = MaterialTheme.typography.body1
                    )
                    Text(
                        text = stringResource(id = R.string.wrong_group_number),
                        modifier = Modifier.padding(
                            top = dimensionResource(
                                id = R.dimen._12dp
                            ),
                            start = dimensionResource(id = R.dimen._8dp)
                        ),
                        style = MaterialTheme.typography.body1
                    )
                    Text(
                        text = stringResource(id = R.string.input_group_format),
                        modifier = Modifier.padding(
                            top = dimensionResource(
                                id = R.dimen._4dp
                            ),
                            start = dimensionResource(id = R.dimen._24dp)
                        ),
                        style = MaterialTheme.typography.body2
                    )
                    Text(
                        text = stringResource(id = R.string.bad_internet_connection),
                        modifier = Modifier.padding(
                            top = dimensionResource(
                                id = R.dimen._8dp
                            ),
                            start = dimensionResource(id = R.dimen._8dp)
                        ),
                        style = MaterialTheme.typography.body1
                    )
                    Text(
                        text = stringResource(id = R.string.error_on_the_server),
                        modifier = Modifier.padding(
                            top = dimensionResource(
                                id = R.dimen._8dp
                            ),
                            start = dimensionResource(id = R.dimen._8dp)
                        ),
                        style = MaterialTheme.typography.body1
                    )
                    ElderButton(
                        onClick = onTryAgainClicked,
                        modifier = Modifier
                            .padding(top = 28.dp)
                            .fillMaxWidth()
                            .align(Alignment.CenterHorizontally)
                    ) {
                        Text(text = stringResource(id = R.string.try_again))
                    }
                }
            }
        }
    }

}


@Preview
@Composable
fun PreviewErrorScreen() {
    ElderTheme {
        ErrorDialog(onTryAgainClicked = {}, true)
    }
}