package com.ustudent.app.ui.module.home

import androidx.lifecycle.viewModelScope
import com.ustudent.app.R
import com.ustudent.app.domain.model.*
import com.ustudent.app.domain.model.group.Group
import com.ustudent.app.domain.usecase.base.UseCase
import com.ustudent.app.domain.usecase.feature.group.GetGroupUseCase
import com.ustudent.app.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getGroupUseCase: GetGroupUseCase
) : BaseViewModel() {

    private val _group = MutableStateFlow<Group?>(null)
    val group: StateFlow<Group?> = _group

    val schedule = Schedule(
        firstWeek = Week(
            monday = Day(
                firstLesson = Lesson(
                    name = "Программная инженерия",
                    room = "310эк",
                    teachers = listOf("Ефанова Наталья Владимировна"),
                    isLection = true
                ),
                secondLesson = Lesson(
                    name = "Основы автоматизации бухгалтерского учета",
                    room = "8эк",
                    teachers = listOf("Тюнин Евгений Борисович")
                ),
                thirdLesson = null,
                fourthLesson = null,
                fifthLesson = null,
                sixthLesson = null
            ),
            tuesday = Day(
                firstLesson = null,
                secondLesson = Lesson(
                    name = "Элективные курсы по физической культуре и спорту",
                    room = "спорт",
                    teachers = null
                ),
                thirdLesson = Lesson(
                    name = "Инструментальные средства моделирования бизнес-процессов",
                    room = "637гл",
                    teachers = listOf("Яхонтова Ирина Михайловна"),
                    isLection = true
                ),
                fourthLesson = Lesson(
                    name = "Методы и средства моделирования в экономике",
                    room = "213эк",
                    teachers = listOf("Косников Сергей Николаевич", "Затонская Ирина Викторовна")
                ),
                fifthLesson = Lesson(
                    name = "Имитационное моделирование",
                    room = "213эк",
                    teachers = listOf("Франциско Ольга Юрьевна, Осенний Виталий Витальевич")
                ),
                sixthLesson = Lesson(
                    name = "Программная инженерия",
                    room = "215эк",
                    teachers = listOf("Иванова Елена Александровна")
                )
            ),
            wednesday = Day(
                firstLesson = Lesson(
                    name = "Программная инженерия",
                    room = "216эк",
                    teachers = listOf("Ефанова Наталья Владимировна")
                ),
                secondLesson = Lesson(
                    name = "Инструментальные средства моделирования бизнес-процессов",
                    room = "215эк, 216эк",
                    teachers = listOf("Яхонтова Ирина Михайловна", "Нилова Надежда Михайловна")
                ),
                thirdLesson = Lesson(
                    name = "Методы и средства моделирования в экономике",
                    room = "8эк",
                    teachers = listOf("Косников Сергей Николаевич"),
                    isLection = true
                ),
                fourthLesson = Lesson(
                    name = "Методы хранения и анализа данных",
                    room = "3эк",
                    teachers = listOf("Лещенко Кирилл Денисович")
                ),
                fifthLesson = null,
                sixthLesson = null
            ),
            thursday = Day(
                firstLesson = Lesson(
                    name = "Иностранный язык",
                    room = "628гл, 610гл",
                    teachers = listOf(
                        "Хитарова Татьяна Александровна",
                        "Погребняк Наталья Владимировна"
                    )
                ),
                secondLesson = Lesson(
                    name = "Методы хранения и анализа данных",
                    room = "219гл",
                    teachers = listOf("Кумратова Альфира Менлигуловна"),
                    isLection = true
                ),
                thirdLesson = Lesson(
                    name = "Проектирование информационных систем",
                    room = "307эк",
                    teachers = listOf("Кондратьев Валерий Юрьевич")
                ),
                fourthLesson = Lesson(
                    name = "Проектирование информационных систем",
                    room = "229зр",
                    teachers = listOf("Кондратьев Валерий Юрьевич"),
                    isLection = true
                ),
                fifthLesson = null,
                sixthLesson = null
            ),
            friday = Day(
                firstLesson = null,
                secondLesson = Lesson(
                    name = "Элективные курсы по физической культуре и спорту",
                    room = "спорт",
                    teachers = null
                ),
                thirdLesson = Lesson(
                    name = "Менеджмент",
                    room = "315зр",
                    teachers = listOf("Саенко Ирина Ивановна")
                ),
                fourthLesson = null,
                fifthLesson = null,
                sixthLesson = null
            ),
            saturday = Day(
                firstLesson = null,
                secondLesson = null,
                thirdLesson = null,
                fourthLesson = null,
                fifthLesson = null,
                sixthLesson = null
            ),
        ), secondWeek = Week(
            monday = Day(
                firstLesson = Lesson(
                    name = "Основы автоматизации бухгалтерского учета",
                    room = "633гл",
                    teachers = listOf("Тюнин Евгений Борисович"),
                    isLection = true
                ),
                secondLesson = Lesson(
                    name = "Элективные курсы по физической культуре и спорту",
                    room = "спорт",
                    teachers = null
                ),
                thirdLesson = null,
                fourthLesson = null,
                fifthLesson = null,
                sixthLesson = null
            ),
            tuesday = Day(
                firstLesson = null,
                secondLesson = Lesson(
                    name = "Методы хранения и анализа данных",
                    room = "223зр",
                    teachers = listOf("Кумратова Альфира Менлигуловна"),
                    isLection = true
                ),
                thirdLesson = Lesson(
                    name = "Инструментальные средства моделирования бизнес-процессов",
                    room = "215эк, 216эк",
                    teachers = listOf("Яхонтова Ирина Михайловна", "Нилова Надежда Михайловна")
                ),
                fourthLesson = Lesson(
                    name = "Программная инженерия",
                    room = "403",
                    teachers = listOf("Ефанова Наталья Владимировна"),
                    isLection = true
                ),
                fifthLesson = Lesson(
                    name = "Программная инженерия",
                    room = "216эк",
                    teachers = listOf("Ефанова Наталья Владимировна")
                ),
                sixthLesson = null
            ), wednesday = Day(
                firstLesson = null,
                secondLesson = null,
                thirdLesson = Lesson(
                    name = "Инструментальные средства моделирования бизнес-процессов",
                    room = "221гл",
                    teachers = listOf("Яхонтова Ирина Михайловна"),
                    isLection = true
                ),
                fourthLesson = Lesson(
                    name = "Методы и средства моделирования в экономике",
                    room = "16эк, 15эк",
                    teachers = listOf("Затонская Ирина Викторовна", "Косников Сергей Николаевич")
                ),
                fifthLesson = Lesson(
                    name = "Проектирование информационных систем",
                    room = "205эк, 207эк",
                    teachers = listOf("Кондратьев Валерий Юрьевич")
                ),
                sixthLesson = Lesson(
                    name = "Программная инженерия",
                    room = "215эк",
                    teachers = listOf("Иванова Елена Александровна")
                )
            ), thursday = Day(
                firstLesson = null,
                secondLesson = Lesson(
                    name = "Имитационное моделирование",
                    room = "414зоо",
                    teachers = listOf("Франциско Ольга Юрьевна"),
                    isLection = true
                ),
                thirdLesson = Lesson(
                    name = "Методы хранения и анализа данных",
                    room = "3эк",
                    teachers = listOf("Лещенко Кирилл Денисович")
                ),
                fourthLesson = Lesson(
                    name = "Имитационное моделирование",
                    room = "210эк",
                    teachers = listOf("Франциско Ольга Юрьевна, Осенний Виталий Витальевич")
                ),
                fifthLesson = Lesson(
                    name = "Основы автоматизации бухгалтерского учета",
                    room = "206эк, 205эк",
                    teachers = listOf("Тюнин Евгений Борисович")
                ),
                sixthLesson = null
            ), friday = Day(
                firstLesson = Lesson(
                    name = "Менеджмент",
                    room = "315зр",
                    teachers = listOf("Саенко Ирина Ивановна")
                ),
                secondLesson = Lesson(
                    name = "Иностранный язык",
                    room = "628гл, 610гл",
                    teachers = listOf(
                        "Хитарова Татьяна Александровна",
                        "Погребняк Наталья Владимировна"
                    )
                ),
                thirdLesson = Lesson(
                    name = "Менеджмент",
                    room = "637гл",
                    teachers = listOf("Калитко Светлана Алексеевна"),
                    isLection = true
                ),
                fourthLesson = Lesson(
                    name = "Проектирование информационных систем",
                    room = "403эк",
                    teachers = listOf(
                        "Кондратьев Валерий Юрьевич"
                    ),
                    isLection = true
                ),
                fifthLesson = null,
                sixthLesson = null
            ), saturday = Day(
                firstLesson = null,
                secondLesson = null,
                thirdLesson = null,
                fourthLesson = null,
                fifthLesson = null,
                sixthLesson = null
            )
        )
    )

    init {
        getGroupUseCase.execute(
            params = UseCase.None(),
            scope = viewModelScope,
            onSuccess = { newGroup ->
                _group.update {
                    newGroup
                }
            },
            onError = {
                _singleMessageId.postValue(R.string.unknown_error)
            }
        )
    }
}