package com.ustudent.app.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ustudent.app.utils.SingleLiveEvent

open class BaseViewModel : ViewModel() {

    protected val _singleMessageId = SingleLiveEvent<Int>()
    val singleMessageId: LiveData<Int> = _singleMessageId
}