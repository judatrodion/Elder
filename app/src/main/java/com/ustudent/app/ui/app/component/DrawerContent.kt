package com.ustudent.app.ui.app.component

import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavDestination
import com.ustudent.app.ui.base.navigation.TopLevelDestination
import com.ustudent.app.ui.module.notifications.NotificationsDestination

@Composable
fun UStudentDrawer(
    notificationCount: Int,
    currentItem: NavDestination?,
    drawerItems: List<TopLevelDestination>,
    onNavigateToDestination: (dest: TopLevelDestination) -> Unit,
    quote: String,
    onQuoteLike: (Int, Boolean) -> Unit,
    onSuggestQuote: () -> Unit
) {
    Column {
        Spacer(modifier = Modifier.height(24.dp))
        DrawerItems(
            notificationCount = notificationCount,
            destinations = drawerItems,
            currentRoute = currentItem?.route,
            onItemClick = onNavigateToDestination
        )
        Spacer(modifier = Modifier.height(14.dp))
        Divider(
            modifier = Modifier.fillMaxWidth(),
            color = colorResource(id = com.ustudent.designsystem.R.color.grey_divider)
        )
        QuoteSection(quote, onQuoteLike, onSuggestQuote)
    }
}

@Composable
private fun DrawerItems(
    notificationCount: Int,
    destinations: List<TopLevelDestination>,
    currentRoute: String?,
    onItemClick: (dest: TopLevelDestination) -> Unit
) {
    Column(verticalArrangement = Arrangement.SpaceBetween, modifier = Modifier) {
        destinations.forEach { destination ->
            DrawerNavigationItem(
                icon = destination.selectedIcon.getIcon(),
                title = stringResource(destination.textId),
                count = if (destination.route == NotificationsDestination.route) notificationCount else 0,
                active = destination.route == currentRoute,
                onItemClick = {
                    onItemClick(destination)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp)
            )
        }
    }
}

@Composable
private fun QuoteSection(
    quote: String,
    onQuoteLike: (Int, Boolean) -> Unit,
    onSuggestQuote: () -> Unit
) {
    Text("Цитаты великих студентов")
}