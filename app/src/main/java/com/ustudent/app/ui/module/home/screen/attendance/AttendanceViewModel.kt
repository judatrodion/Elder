package com.ustudent.app.ui.module.home.screen.attendance

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.viewModelScope
import com.ustudent.app.domain.model.attendance.AttendanceReport
import com.ustudent.app.domain.model.attendance.AttendanceStudentModel
import com.ustudent.app.domain.model.attendance.ReportLessonModel
import com.ustudent.app.domain.model.attendance.toAttendanceStudent
import com.ustudent.app.domain.usecase.base.UseCase
import com.ustudent.app.domain.usecase.feature.group.GetGroupNameUseCase
import com.ustudent.app.domain.usecase.feature.students.GetStudentsUseCase
import com.ustudent.app.ui.base.BaseViewModel
import com.ustudent.app.ui.module.home.screen.attendance.model.AttendanceFilter
import com.ustudent.app.ui.module.home.screen.attendance.model.LessonAttendanceUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.todayIn
import javax.inject.Inject

@HiltViewModel
class AttendanceViewModel @Inject constructor(
    getStudentsUseCase: GetStudentsUseCase,
    getGroupNameUseCase: GetGroupNameUseCase
) : BaseViewModel() {

    private val _sendReportEvent = MutableSharedFlow<AttendanceReport>()
    val sendReportEvent = _sendReportEvent.asSharedFlow()

    private val _uiState: MutableStateFlow<AttendanceScreenState> =
        MutableStateFlow(AttendanceScreenState())
    val uiState = _uiState.asStateFlow()

    private var groupName: String? = null

    private val _students: MutableStateFlow<SnapshotStateList<AttendanceStudentModel>> =
        MutableStateFlow(mutableStateListOf())
    val students: StateFlow<List<AttendanceStudentModel>> = _students

    private val _date: MutableStateFlow<LocalDate> =
        MutableStateFlow(Clock.System.todayIn(TimeZone.currentSystemDefault()))
    val date = _date.asStateFlow()

    private val _todayLessons =
        MutableStateFlow(
            listOf(
                LessonAttendanceUiState(
                    1, "Английский язык"
                ),
                LessonAttendanceUiState(
                    2,
                    "Математический анализ и другие разделы математики"
                ),
                LessonAttendanceUiState(
                    4, "Программирование"
                ),
                LessonAttendanceUiState(
                    5, "Экономика"
                ),
            )
        )

    val todayLessons = _todayLessons.asStateFlow()

    private val _lesson =
        MutableStateFlow(
            LessonAttendanceUiState(
                2,
                "Математический анализ и другие разделы математики"
            )
        )
    val lesson = _lesson.asStateFlow()

    init {
        getStudentsUseCase.execute(
            params = UseCase.None(),
            scope = viewModelScope,
            onSuccess = { studentsResult ->
                _students.emit(
                    studentsResult.map { student ->
                        student.toAttendanceStudent()
                    }.toMutableStateList()
                )
            },
            onError = {

            }
        )
        getGroupNameUseCase.execute(
            params = UseCase.None(),
            scope = viewModelScope,
            onSuccess = {
                groupName = it
            },
            onError = {
                throw Exception("Нет данных о группе")
            }
        )
    }

    fun onStudentClick(studentIndex: Int) {
        _students.value[studentIndex] = _students.value[studentIndex].copy(
            isAttending = !_students.value[studentIndex].isAttending,
            hasReason = false
        )
    }

    fun onStudentReasonEdit(index: Int, reason: String) {
        _students.value[index] = _students.value[index].copy(
            reasonOfMissing = reason
        )
    }

    fun onEditStarted() {
        _uiState.update {
            it.copy(keyboardVisible = true)
        }
    }

    fun onEditFinished() {
        _uiState.update {
            it.copy(keyboardVisible = false)
        }
    }

    fun onCheckAllClick(check: Boolean) {
        viewModelScope.launch {
            _students.emit(
                students.value.map { student ->
                    student.copy(isAttending = check, hasReason = false)
                }.toMutableStateList()
            )
        }
    }

    fun onSaveClick() {

    }

    fun onAddReason(index: Int) {
        _students.value[index] = _students.value[index].copy(hasReason = true)
    }

    fun onDeleteReason(index: Int) {
        _students.value[index] = _students.value[index].copy(hasReason = false)
    }

    fun onSelectDateClick(select: Boolean) {
        _uiState.update {
            it.copy(showSelectDateDialog = select)
        }
    }

    fun onSelectLessonClick(select: Boolean) {
        _uiState.update {
            it.copy(showSelectLessonDialog = select)
        }
    }

    fun onSelectLesson(lessonAttendanceUiState: LessonAttendanceUiState) {
        _uiState.update {
            it.copy(showSelectLessonDialog = false)
        }
        _lesson.update {
            lessonAttendanceUiState
        }
    }

    fun onSelectDate(date: LocalDate) {
        _date.update { date }
        _uiState.update {
            it.copy(showSelectDateDialog = false)
        }
    }

    fun onShowSendDialog(show: Boolean) {
        _uiState.update {
            it.copy(showSendReportDialog = show)
        }
    }

    fun onSendReport(attendanceFilter: AttendanceFilter) {
        _uiState.update {
            it.copy(showSendReportDialog = false)
        }
        viewModelScope.launch {
            _sendReportEvent.emit(
                AttendanceReport(
                    groupName = groupName!!, filter = attendanceFilter, lesson = ReportLessonModel(
                        number = lesson.value.number,
                        date = date.value,
                        lessonName = lesson.value.lessonName
                    ), students = students.value
                )
            )
        }
    }
}
