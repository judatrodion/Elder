package com.ustudent.app.ui.module.home.component

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavDestination
import com.ustudent.app.designsystem.theme.BottomNavigationTextStyle
import com.ustudent.app.ui.module.home.destination.HomeScreenTopDestinations

@Composable
fun HomeBottomBar(
    topDestinations: List<HomeScreenTopDestinations>,
    onTabSelected: (HomeScreenTopDestinations) -> Unit,
    currentDestination: NavDestination?
) {
    BottomNavigation(
        elevation = 8.dp,
        backgroundColor = MaterialTheme.colors.surface,
        contentColor = MaterialTheme.colors.onSurface
    ) {
        topDestinations.forEach { screen ->
            BottomNavigationItem(
                selected = currentDestination?.route == screen.route,
                onClick = { onTabSelected(screen) },
                icon = {
                    Icon(
                        imageVector = screen.selectedIcon.getIcon(),
                        contentDescription = null
                    )
                },
                label = {
                    Text(
                        stringResource(id = screen.titleResourceId),
                        style = BottomNavigationTextStyle
                    )
                },
                selectedContentColor = MaterialTheme.colors.primary,
                unselectedContentColor = MaterialTheme.colors.onSurface,
                alwaysShowLabel = true
            )
        }
    }
}