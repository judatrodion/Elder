package com.ustudent.app.ui.module.home.screen.attendance

import com.ustudent.app.R
import com.ustudent.app.ui.base.navigation.UStudentNavigationDestination

object AttendanceDestination: UStudentNavigationDestination {
    override val route: String
        get() = "attendance_destination"
    override val textId: Int
        get() = R.string.attendance
}