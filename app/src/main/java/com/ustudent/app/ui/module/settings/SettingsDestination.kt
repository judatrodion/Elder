package com.ustudent.app.ui.module.settings

import com.ustudent.app.R
import com.ustudent.app.ui.base.navigation.UStudentNavigationDestination

object SettingsDestination : UStudentNavigationDestination {
    override val route: String = "settings_destination"
    override val textId: Int
        get() = R.string.settings
}