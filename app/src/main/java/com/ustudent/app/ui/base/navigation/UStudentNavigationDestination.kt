package com.ustudent.app.ui.base.navigation

interface UStudentNavigationDestination {
    val route: String
    val textId: Int
}
