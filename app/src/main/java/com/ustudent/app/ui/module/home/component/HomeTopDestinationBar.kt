package com.ustudent.app.ui.module.home.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.ustudent.app.R
import com.ustudent.app.designsystem.component.ElderTopBar

@Composable
fun HomeTopDestinationBar(
    title: String,
    onNavigationIconClicked: () -> Unit,
    actions: @Composable RowScope.() -> Unit = {}
) {
    ElderTopBar(
        title = title,
        navigationIcon = {
            Row(
                modifier = Modifier.clickable(
                    indication = null,
                    interactionSource = MutableInteractionSource(),
                    onClick = onNavigationIconClicked
                ), verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_menu),
                    contentDescription = null,
                    tint = Color.Unspecified
                )
                Icon(
                    painter = painterResource(id = R.drawable.ic_navigation_icon),
                    contentDescription = null,
                    tint = MaterialTheme.colors.primary
                )
            }
        },
        actions = actions,
        contentPadding = PaddingValues(0.dp)
    )
}