package com.ustudent.app.ui.app

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.ustudent.app.designsystem.icon.Icon
import com.ustudent.app.designsystem.icon.UstudentIcons
import com.ustudent.app.ui.base.navigation.TopLevelDestination
import com.ustudent.app.ui.base.navigation.UStudentNavigationDestination
import com.ustudent.app.ui.module.home.HomeDestination
import com.ustudent.app.ui.module.notifications.NotificationsDestination
import com.ustudent.app.ui.module.office.OfficeDestination
import com.ustudent.app.ui.module.settings.SettingsDestination

@Composable
fun rememberUStudentAppState(
    navController: NavHostController = rememberNavController()
): UStudentAppState {
    return remember(navController) {
        UStudentAppState(navController)
    }
}

@Stable
class UStudentAppState(
    val navController: NavHostController
) {
    val currentDestination: NavDestination?
        @Composable get() = navController
            .currentBackStackEntryAsState().value?.destination

    val topLevelDestinations: List<TopLevelDestination> = listOf(
        TopLevelDestination(
            route = HomeDestination.route,
            selectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Home),
            unselectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Home),
            textId = HomeDestination.textId
        ),
        TopLevelDestination(
            route = OfficeDestination.route,
            selectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Person),
            unselectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Person),
            textId = OfficeDestination.textId
        ),
        TopLevelDestination(
            route = SettingsDestination.route,
            selectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Settings),
            unselectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Settings),
            textId = SettingsDestination.textId
        ),
        TopLevelDestination(
            route = NotificationsDestination.route,
            selectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Notification),
            unselectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Notification),
            textId = NotificationsDestination.textId
        )
    )

    fun navigate(destination: UStudentNavigationDestination, route: String? = null) {
        if (destination is TopLevelDestination) {
            navController.popBackStack()
            navController.navigate(route ?: destination.route) {
                // Pop up to the start destination of the graph to
                // avoid building up a large stack of destinations
                // on the back stack as users select items
                popUpTo(navController.graph.findStartDestination().id) {
                    inclusive = true
                }
                // Avoid multiple copies of the same destination when
                // reselecting the same item
                launchSingleTop = true
                // Restore state when reselecting a previously selected item
                restoreState = true
            }
        } else {
            navController.navigate(route ?: destination.route)
        }
    }

    fun onBackClick() {
        navController.popBackStack()
    }
}