package com.ustudent.app.ui.module.home.screen.attendance.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.ustudent.app.R
import com.ustudent.app.ui.module.home.screen.attendance.model.LessonAttendanceUiState

@Composable
fun LessonItem(
    lessonAttendanceUiState: LessonAttendanceUiState,
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    Column(modifier = modifier.clickable { onClick() }) {
        Text(
            text = lessonAttendanceUiState.number.toString() + " ${stringResource(id = R.string.lesson).lowercase()}",
            fontSize = 20.sp,
            fontWeight = FontWeight.Normal,
            modifier = Modifier.padding(horizontal = 16.dp).padding(top = 8.dp)
        )
        Text(
            text = lessonAttendanceUiState.lessonName,
            style = MaterialTheme.typography.caption,
            color = MaterialTheme.colors.onSurface.copy(0.7f),
            fontWeight = FontWeight.Light,
            modifier = Modifier.padding(horizontal = 16.dp).padding(bottom = 8.dp)
        )
    }
}