package com.ustudent.app.ui.module.home.screen.hometask

import com.ustudent.app.R
import com.ustudent.app.ui.base.navigation.UStudentNavigationDestination

object HomeTaskDestination : UStudentNavigationDestination {
    override val route: String
        get() = "hometask_destination"
    override val textId: Int
        get() = R.string.tasks
}