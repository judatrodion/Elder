package com.ustudent.app.ui.module.home.screen.events

import com.ustudent.app.R
import com.ustudent.app.ui.base.navigation.UStudentNavigationDestination

object EventsDestination: UStudentNavigationDestination {
    override val route: String
        get() = "events_destination"
    override val textId: Int
        get() = R.string.events
}