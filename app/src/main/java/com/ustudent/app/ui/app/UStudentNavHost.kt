package com.ustudent.app.ui.app

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.ustudent.app.ui.module.home.HomeDestination
import com.ustudent.app.ui.module.home.HomeModule
import com.ustudent.app.ui.module.notifications.NotificationsDestination
import com.ustudent.app.ui.module.office.OfficeDestination
import com.ustudent.app.ui.module.settings.SettingsDestination

@Composable
fun UStudentNavHost(
    navController: NavHostController,
    modifier: Modifier = Modifier,
    startDestination: String = HomeDestination.route,
    onNavigationIconClick: () -> Unit
) {
    NavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = modifier
    ) {
        composable(HomeDestination.route) {
            HomeModule(onNavigationIconClick = onNavigationIconClick)
        }
        composable(OfficeDestination.route) {
            Surface(Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
                Text(OfficeDestination.route)
            }
        }
        composable(SettingsDestination.route) {
            Surface(Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
                Text(SettingsDestination.route)
            }
        }
        composable(NotificationsDestination.route) {
            Surface(Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
                Text(NotificationsDestination.route)
            }
        }
    }
}
