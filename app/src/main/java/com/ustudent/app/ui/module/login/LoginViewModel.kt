package com.ustudent.app.ui.module.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ustudent.app.domain.model.group.Group
import com.ustudent.app.domain.usecase.feature.group.ParseGroupUseCase
import com.ustudent.app.domain.usecase.feature.group.SaveGroupUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val parseGroupUseCase: ParseGroupUseCase,
    private val saveGroupUseCase: SaveGroupUseCase
) : ViewModel() {

    private val _loginUiState = MutableStateFlow(
        LoginUiState(
            "",
            true,
            null,
            emptyList(),
            null,
            isLoading = false
        )
    )
    val loginState: StateFlow<LoginUiState> = _loginUiState

    private val maxGroupNameLength = 9

    fun onGroupNameChange(value: String) {
        if (value.length >= maxGroupNameLength) {
            return
        }
        _loginUiState.update { state ->
            state.copy(
                groupNumber = value,
                nameIsValid = true
            )
        }
    }

    private fun saveGroup(group: Group) {
        saveGroupUseCase.execute(
            params = SaveGroupUseCase.Params(
                group
            ),
            scope = viewModelScope,
            onSuccess = {
                _loginUiState.update { state ->
                    state.copy(
                        savedSuccess = true
                    )
                }
            },
            onError = {
                _loginUiState.update { state ->
                    state.copy(
                        savedSuccess = false
                    )
                }
            }
        )
    }

    fun resetGroupSavedState() {
        _loginUiState.update {
            it.copy(
                savedSuccess = null
            )
        }
    }

    fun onContinueClicked(): Boolean {
        return if (isGroupNameValid()) {
            loadGroup()
            true
        } else {
            _loginUiState.update {
                it.copy(nameIsValid = false)
            }
            false
        }
    }

    private fun loadGroup() {
        _loginUiState.update {
            it.copy(
                isLoading = true
            )
        }
        parseGroupUseCase.execute(
            params = _loginUiState.value.groupNumber,
            scope = viewModelScope,
            onSuccess = {
                _loginUiState.update { currentState ->
                    currentState.copy(
                        parsedSuccess = true,
                        students = it,
                        isLoading = false
                    )
                }
                saveGroup(
                    _loginUiState.value.getGroup()
                )
            },
            onError = {
                _loginUiState.update { currentState ->
                    currentState.copy(
                        parsedSuccess = false,
                        isLoading = false
                    )
                }
            }
        )
    }

    private fun isGroupNameValid(): Boolean {
        _loginUiState.update {
            it.copy(groupNumber = it.groupNumber.trim())
        }
        return _loginUiState.value.groupNumber.isNotBlank()
    }

    fun onCloseDialog() {
        _loginUiState.update {
            it.copy(parsedSuccess = null)
        }
    }
}