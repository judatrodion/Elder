package com.ustudent.app.ui.module.home.destination

import androidx.annotation.StringRes
import com.ustudent.app.designsystem.icon.Icon
import com.ustudent.app.designsystem.icon.UstudentIcons
import com.ustudent.app.ui.module.home.screen.attendance.AttendanceDestination
import com.ustudent.app.ui.module.home.screen.events.EventsDestination
import com.ustudent.app.ui.module.home.screen.hometask.HomeTaskDestination
import com.ustudent.app.ui.module.home.screen.schedule.ScheduleDestination

data class HomeScreenTopDestinations(
    val route: String,
    @StringRes val titleResourceId: Int,
    val selectedIcon: Icon,
    val unselectedIcon: Icon
)

val homeModuleDestinations = listOf(
    HomeScreenTopDestinations(
        route = ScheduleDestination.route,
        titleResourceId = ScheduleDestination.textId,
        selectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Schedule),
        unselectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Schedule)
    ),
    HomeScreenTopDestinations(
        route = AttendanceDestination.route,
        titleResourceId = AttendanceDestination.textId,
        selectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Edit),
        unselectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Edit)
    ),
    HomeScreenTopDestinations(
        route = HomeTaskDestination.route,
        titleResourceId = HomeTaskDestination.textId,
        selectedIcon = Icon.DrawableResourceIcon(UstudentIcons.HomeWork),
        unselectedIcon = Icon.DrawableResourceIcon(UstudentIcons.HomeWork)
    ),
    HomeScreenTopDestinations(
        route = EventsDestination.route,
        titleResourceId = EventsDestination.textId,
        selectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Events),
        unselectedIcon = Icon.DrawableResourceIcon(UstudentIcons.Events)
    )
)