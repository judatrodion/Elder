package com.ustudent.app.ui.module.home.screen.attendance

import androidx.compose.animation.*
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.ustudent.app.designsystem.component.ElderTopBar
import com.ustudent.app.designsystem.icon.UstudentIcons
import com.ustudent.designsystem.R

data class AttendanceScreenState(
    val keyboardVisible: Boolean = false,
    val showSelectDateDialog: Boolean = false,
    val showSelectLessonDialog: Boolean = false,
    val showSendReportDialog: Boolean = false,
    val sendReport: Boolean = false
) {

    @Composable
    fun DefaultTopBar(onNavigationIconClick: () -> Unit) {
        ElderTopBar(
            title = "Посещаемость",
            navigationIcon = {
                Row(verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.clickable(
                        interactionSource = MutableInteractionSource(),
                        indication = null
                    ) { onNavigationIconClick() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_menu),
                        contentDescription = null,
                        tint = Color.Unspecified
                    )
                    Spacer(modifier = Modifier.width(4.dp))
                    Icon(
                        painter = painterResource(id = R.drawable.ic_elder),
                        contentDescription = null,
                        modifier = Modifier.size(25.dp),
                        tint = Color.Unspecified
                    )
                }
            },
            contentPadding = PaddingValues(start = 0.dp, end = 8.dp)
        )
    }

    @Composable
    fun EditTopBar(onExitEditClick: () -> Unit, onDoneEditClick: () -> Unit) {
        ElderTopBar(
            title = "",
            navigationIcon = {
                IconButton(onClick = { onExitEditClick() }) {
                    Icon(
                        painter = painterResource(id = UstudentIcons.NavigateUp),
                        contentDescription = null
                    )
                }
            }, actions = {
                IconButton(onClick = { onDoneEditClick() }) {
                    Icon(
                        painter = painterResource(id = UstudentIcons.Done),
                        contentDescription = null
                    )
                }
            }
        )
    }

    @OptIn(ExperimentalAnimationApi::class)
    @Composable
    fun TopBar(
        onNavigationIconClick: () -> Unit,
        onExitEditClick: () -> Unit,
        onDoneEditClick: () -> Unit
    ) {
        AnimatedContent(targetState = keyboardVisible, transitionSpec = {
            fadeIn() with fadeOut()
        }) { targetState: Boolean ->
            if (targetState) {
                EditTopBar(onExitEditClick = onExitEditClick, onDoneEditClick = onDoneEditClick)
            } else {
                DefaultTopBar(onNavigationIconClick)
            }
        }
    }
}
