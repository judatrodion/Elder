package com.ustudent.app.ui.module.home.screen.attendance

import android.content.Context
import android.content.Intent
import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.*
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.hilt.navigation.compose.hiltViewModel
import com.ustudent.app.designsystem.component.DatePicker
import com.ustudent.app.designsystem.component.SurfaceButton
import com.ustudent.app.designsystem.component.UStudentIconButton
import com.ustudent.app.designsystem.component.UStudentOutlinedButton
import com.ustudent.app.designsystem.icon.UstudentIcons
import com.ustudent.app.designsystem.theme.BottomSheetLabel
import com.ustudent.app.designsystem.theme.ElderTheme
import com.ustudent.app.domain.model.attendance.AttendanceReport
import com.ustudent.app.ui.module.home.screen.attendance.component.LessonItem
import com.ustudent.app.ui.module.home.screen.attendance.component.StudentItem
import com.ustudent.app.ui.module.home.screen.attendance.model.AttendanceFilter
import com.ustudent.app.ui.module.home.screen.attendance.model.LessonAttendanceUiState
import com.ustudent.app.ui.utils.Keyboard
import com.ustudent.app.ui.utils.keyboardAsState
import com.ustudent.app.utils.formatToText
import com.ustudent.designsystem.R
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.datetime.LocalDate

@OptIn(
    ExperimentalMaterialApi::class,
    ExperimentalComposeUiApi::class
)
@Composable
fun AttendanceScreen(
    viewModel: AttendanceViewModel = hiltViewModel(),
    onNavigationIconClick: () -> Unit,
    onBottomNavigationVisibilityChange: (Boolean) -> Unit
) {
    val uiState = viewModel.uiState.collectAsState()
    val students by viewModel.students.collectAsState()
    val allChecked by derivedStateOf {
        if (students.isEmpty()) false else students.find { !it.isAttending } == null
    }
    val peopleCount by derivedStateOf {
        students.filter { it.isAttending }.size
    }

    val bottomSheetHeight =
        if (uiState.value.keyboardVisible) 0.dp else 115.dp
    val bottomSheetScaffoldState = rememberBottomSheetScaffoldState()
    val bottomSheetState = bottomSheetScaffoldState.bottomSheetState
    val bottomSheetCollapsed by derivedStateOf {
        if (bottomSheetState.direction == -1f) {
            return@derivedStateOf false
        }
        bottomSheetState.isCollapsed || bottomSheetState.direction == 1f
    }

    val keyboardController = LocalSoftwareKeyboardController.current
    val focusManager = LocalFocusManager.current
    val keyboardState by keyboardAsState()
    LaunchedEffect(keyboardState) {
        when (keyboardState) {
            Keyboard.Closed -> viewModel.onEditFinished()
            Keyboard.Opened -> {
                viewModel.onEditStarted()
                launch { bottomSheetState.collapse() }
            }
        }
    }

    onBottomNavigationVisibilityChange(!uiState.value.keyboardVisible)
    SelectDateDialog(
        show = uiState.value.showSelectDateDialog,
        onSelect = viewModel::onSelectDate,
        onDismiss = viewModel::onSelectDateClick
    )
    SelectLessonDialog(
        uiState.value.showSelectLessonDialog,
        lessons = viewModel.todayLessons.collectAsState().value,
        onDismiss = { viewModel.onSelectLessonClick(false) },
        onSelect = viewModel::onSelectLesson,
    )
    SendReportDialog(
        uiState.value.showSendReportDialog,
        onFilterChosen = viewModel::onSendReport,
        onShowDialog = viewModel::onShowSendDialog
    )
    val context = LocalContext.current
    LaunchedEffect(Unit) {
        viewModel.sendReportEvent.collectLatest {
            sendReport(it, context)
        }
    }

    BottomSheetScaffold(
        scaffoldState = bottomSheetScaffoldState,
        sheetContent = {
            Spacer(
                modifier = Modifier
                    .height(2.dp)
            )
            SheetContent(
                bottomSheetCollapsed = bottomSheetCollapsed,
                allChecked = allChecked,
                onSendClick = viewModel::onShowSendDialog,
                onSaveClick = viewModel::onSaveClick,
                onCheckAllClick = viewModel::onCheckAllClick,
                peopleTotal = students.size,
                peopleCurrent = peopleCount,
                onSelectLessonClick = { viewModel.onSelectLessonClick(true) },
                onSelectDateClick = viewModel::onSelectDateClick,
                date = viewModel.date.collectAsState().value.formatToText(),
                lesson = viewModel.lesson.collectAsState().value
            )
        },
        topBar = {
            uiState.value.TopBar(
                onNavigationIconClick = onNavigationIconClick,
                onExitEditClick = {
                    keyboardController?.hide()
                    focusManager.clearFocus()
                },
                onDoneEditClick = {
                    keyboardController?.hide()
                    focusManager.clearFocus()
                }
            )
        },
        sheetShape = ElderTheme.bottomSheetShape,
        sheetBackgroundColor = Color.Transparent,
        sheetElevation = 1.dp,
        sheetPeekHeight = bottomSheetHeight,
        backgroundColor = MaterialTheme.colors.surface,
    ) { innerPadding ->
        val coroutineScope = rememberCoroutineScope()
        val innerBottomPadding = innerPadding.calculateBottomPadding()
        val imeBottomPadding = WindowInsets.ime.asPaddingValues().calculateBottomPadding()
        val lazyListState = rememberLazyListState()
        LazyColumn(
            contentPadding = PaddingValues(bottom = innerBottomPadding + imeBottomPadding),
            state = lazyListState,
        ) {
            itemsIndexed(
                items = students
            ) { index, student ->
                StudentItem(
                    student = student,
                    index = index,
                    onItemClick = viewModel::onStudentClick,
                    onReasonEdit = viewModel::onStudentReasonEdit,
                    modifier = Modifier.fillMaxWidth(),
                    onClearFocus = {
                        focusManager.clearFocus()
                    },
                    onKeyboardHide = {
                        keyboardController?.hide()
                    },
                    onScrollToItem = {
                        coroutineScope.launch {
                            lazyListState.animateScrollToItem(index)
                        }
                    },
                    onAddReason = viewModel::onAddReason,
                    onDeleteReason = viewModel::onDeleteReason
                )
                Divider(
                    color = colorResource(id = R.color.grey_divider),
                    modifier = Modifier.padding(horizontal = 12.dp)
                )
            }
        }
    }
}

@Composable
private fun SendReportDialog(
    show: Boolean,
    onFilterChosen: (AttendanceFilter) -> Unit,
    onShowDialog: (Boolean) -> Unit
) {
    if (show) {
        Dialog(onDismissRequest = { onShowDialog(false) }) {
            Column(
                modifier = Modifier
                    .wrapContentSize()
                    .background(
                        color = MaterialTheme.colors.surface,
                        shape = RoundedCornerShape(size = 16.dp)
                    )
            ) {
                Text(
                    text = "Выберите фильтр",
                    Modifier.padding(horizontal = 16.dp, vertical = 16.dp)
                )
                Divider(
                    modifier = Modifier.fillMaxWidth(),
                    color = colorResource(id = R.color.grey_divider)
                )
                AttendanceFilter.values().forEach { filter ->
                    Text(
                        text = stringResource(id = filter.stringRes),
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable {
                                onFilterChosen(filter)
                            }
                            .padding(horizontal = 16.dp, vertical = 16.dp)
                    )
                }
            }
        }
    }
}

@Composable
private fun SelectDateDialog(
    show: Boolean,
    onSelect: (LocalDate) -> Unit,
    onDismiss: (Boolean) -> Unit
) {
    if (show) {
        DatePicker(
            onDateSelected = {
                onSelect(it)
            }
        ) {
            onDismiss(false)
        }
    }
}

@Composable
private fun SelectLessonDialog(
    show: Boolean,
    lessons: List<LessonAttendanceUiState>,
    onDismiss: () -> Unit,
    onSelect: (LessonAttendanceUiState) -> Unit
) {
    if (show) {
        Dialog(onDismissRequest = onDismiss) {
            Column(
                modifier = Modifier
                    .wrapContentSize()
                    .background(
                        color = MaterialTheme.colors.surface,
                        shape = RoundedCornerShape(size = 16.dp)
                    )
            ) {
                Spacer(Modifier.height(8.dp))
                lessons.forEach { lesson ->
                    LessonItem(
                        lessonAttendanceUiState = lesson,
                        Modifier
                            .fillMaxWidth()
                    ) { onSelect(lesson) }
                }
                Spacer(Modifier.height(8.dp))
            }
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun SheetContent(
    bottomSheetCollapsed: Boolean,
    allChecked: Boolean,
    date: String,
    lesson: LessonAttendanceUiState,
    onSendClick: (Boolean) -> Unit,
    onSaveClick: () -> Unit,
    onCheckAllClick: (Boolean) -> Unit,
    peopleTotal: Int,
    peopleCurrent: Int,
    onSelectLessonClick: () -> Unit,
    onSelectDateClick: (Boolean) -> Unit
) {
    Surface(
        shape = ElderTheme.bottomSheetShape,
        color = MaterialTheme.colors.surface
    ) {
        Column(
            modifier = Modifier
                .height(220.dp)
                .fillMaxWidth()
        ) {
            Icon(
                painter = painterResource(id = UstudentIcons.BottomSheetLine),
                contentDescription = null,
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 10.dp)
            )
            AnimatedContent(
                targetState = bottomSheetCollapsed,
                transitionSpec = {
                    fadeIn() with fadeOut()
                },
                modifier = Modifier.padding(top = 8.dp, start = 16.dp, end = 16.dp)
            ) { targetState ->
                if (targetState) {
                    CollapsedBottomSheet(
                        modifier = Modifier.fillMaxWidth(),
                        allChecked = allChecked,
                        date = date,
                        lesson = lesson,
                        onSendClick = onSendClick,
                        onCheckAllClick = onCheckAllClick,
                        onSaveClick = onSaveClick,
                        peopleCurrent = peopleCurrent,
                        peopleTotal = peopleTotal
                    )
                } else {
                    ExpandedBottomSheet(
                        modifier = Modifier.fillMaxWidth(),
                        onSendClick = onSendClick,
                        onSaveClick = onSaveClick,
                        onDateClick = onSelectDateClick,
                        onLessonClick = onSelectLessonClick,
                        lesson = lesson,
                        date = date,
                        peopleCurrent = peopleCurrent,
                        peopleTotal = peopleTotal
                    )
                }
            }
        }
    }
}

@Composable
private fun CollapsedBottomSheet(
    modifier: Modifier = Modifier,
    allChecked: Boolean = false,
    peopleCurrent: Int,
    date: String,
    lesson: LessonAttendanceUiState,
    peopleTotal: Int,
    onSendClick: (Boolean) -> Unit,
    onCheckAllClick: (Boolean) -> Unit,
    onSaveClick: () -> Unit,
) {
    ConstraintLayout(modifier = modifier) {
        val (
            dateLabel,
            peopleCount,
            timeLabel,
            saveButton,
            checkButton,
            sendButton) = createRefs()
        TextWithIcon(
            icon = painterResource(id = UstudentIcons.Calendar),
            text = date,
            modifier = Modifier.constrainAs(dateLabel) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                end.linkTo(saveButton.start)
                width = Dimension.fillToConstraints
                height = Dimension.wrapContent
            }
        )
        TextWithIcon(
            icon = painterResource(id = UstudentIcons.Person),
            text = "$peopleCurrent/$peopleTotal",
            modifier = Modifier.constrainAs(peopleCount) {
                top.linkTo(dateLabel.bottom, margin = 6.dp)
                start.linkTo(dateLabel.start)
                width = Dimension.wrapContent
                height = Dimension.wrapContent
            }
        )
        TextWithIcon(
            icon = painterResource(id = UstudentIcons.Time),
            text = lesson.toText(),
            modifier = Modifier.constrainAs(timeLabel) {
                top.linkTo(peopleCount.bottom, margin = 6.dp)
                start.linkTo(dateLabel.start)
                end.linkTo(parent.end)
                width = Dimension.fillToConstraints
                height = Dimension.wrapContent
            }
        )
        UStudentIconButton(
            onClick = { onSendClick(true) },
            modifier = Modifier.constrainAs(sendButton) {
                top.linkTo(dateLabel.top)
                bottom.linkTo(peopleCount.bottom)
                end.linkTo(parent.end)
                width = Dimension.wrapContent
                height = Dimension.wrapContent
            }) {
            Icon(
                painter = painterResource(id = UstudentIcons.Send), contentDescription = "Send",
                tint = MaterialTheme.colors.onPrimary
            )
        }
        val borderColor by animateColorAsState(
            targetValue = if (allChecked) colorResource(id = R.color.green_button_color) else colorResource(
                id = R.color.border_stroke
            )
        )
        val background by animateColorAsState(
            targetValue = if (allChecked) colorResource(id = R.color.green_button_color) else MaterialTheme.colors.surface
        )
        val contentColor by animateColorAsState(
            targetValue = if (allChecked) MaterialTheme.colors.onPrimary else colorResource(
                id = R.color.grey_content_outlined_button
            )
        )
        UStudentIconButton(
            onClick = { onCheckAllClick(!allChecked) },
            borderColor = borderColor,
            color = background,
            modifier = Modifier.constrainAs(checkButton) {
                end.linkTo(sendButton.start, margin = 8.dp)
                top.linkTo(dateLabel.top)
                bottom.linkTo(peopleCount.bottom)
                width = Dimension.wrapContent
                height = Dimension.wrapContent
            }
        ) {
            Icon(
                painter = painterResource(id = UstudentIcons.CheckAll),
                contentDescription = "Check all",
                tint = contentColor
            )
        }
        UStudentIconButton(onClick = onSaveClick,
            color = MaterialTheme.colors.surface,
            borderColor = colorResource(
                id = R.color.border_stroke
            ),
            modifier = Modifier.constrainAs(saveButton) {
                top.linkTo(sendButton.top)
                bottom.linkTo(sendButton.bottom)
                end.linkTo(checkButton.start, margin = 8.dp)
                width = Dimension.wrapContent
                height = Dimension.wrapContent
            }
        ) {
            Icon(
                painter = painterResource(id = UstudentIcons.Save),
                contentDescription = "Check all",
                tint = colorResource(
                    id = R.color.grey_content_outlined_button
                )
            )
        }
    }
}

@Composable
private fun ExpandedBottomSheet(
    modifier: Modifier = Modifier,
    peopleCurrent: Int,
    peopleTotal: Int,
    lesson: LessonAttendanceUiState,
    date: String,
    onSendClick: (Boolean) -> Unit,
    onDateClick: (Boolean) -> Unit,
    onLessonClick: () -> Unit,
    onSaveClick: () -> Unit
) {
    Column(
        modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(
                text = stringResource(id = com.ustudent.app.R.string.date),
                style = MaterialTheme.typography.subtitle1,
            )
            Spacer(modifier = Modifier.width(16.dp))
            SurfaceButton(
                modifier = Modifier.weight(1f),
                onClick = { onDateClick(true) }
            ) {
                Text(
                    text = date, modifier = modifier.weight(1f),
                    fontWeight = FontWeight(500)
                )
                Icon(painterResource(id = UstudentIcons.Calendar), null)
            }
        }
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(
                text = stringResource(id = com.ustudent.app.R.string.lesson),
                style = MaterialTheme.typography.subtitle1,
            )
            Spacer(modifier = Modifier.width(16.dp))
            SurfaceButton(
                modifier = Modifier.weight(1f),
                onClick = onLessonClick
            ) {
                Text(
                    text = lesson.lessonName,
                    modifier = modifier.weight(1f),
                    fontWeight = FontWeight(500),
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
                Spacer(Modifier.width(8.dp))
                Icon(painterResource(id = UstudentIcons.Time), null)
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            TextWithIcon(
                icon = painterResource(id = UstudentIcons.Person),
                text = "$peopleCurrent/$peopleTotal",
                textStyle = BottomSheetLabel.copy(
                    fontSize = 16.sp,
                    lineHeight = 16.sp,
                    letterSpacing = 1.sp
                )
            )
            UStudentOutlinedButton(onClick = onSaveClick, modifier = Modifier.weight(1f)) {
                Icon(painter = painterResource(id = UstudentIcons.Save), contentDescription = null)
                Spacer(modifier = Modifier.width(10.dp))
                Text(text = "Записать")
            }
            UStudentIconButton(onClick = { onSendClick(true) }) {
                Icon(
                    painter = painterResource(id = UstudentIcons.Send), contentDescription = "Send",
                    tint = MaterialTheme.colors.onPrimary
                )
            }
        }
    }
}

@Composable
private fun TextWithIcon(
    icon: Painter,
    text: String,
    modifier: Modifier = Modifier,
    textStyle: TextStyle = BottomSheetLabel
) {
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically) {
        Icon(painter = icon, contentDescription = null, modifier = Modifier.size(22.dp))
        Spacer(Modifier.width(12.dp))
        Text(text = text, style = textStyle, overflow = TextOverflow.Ellipsis, maxLines = 1)
    }
}

private fun sendReport(attendanceReport: AttendanceReport, context: Context) {
    val intent = Intent(Intent.ACTION_SEND).apply {
        type = "text/plain"
        putExtra(Intent.EXTRA_SUBJECT, attendanceReport.getSubject())
        putExtra(Intent.EXTRA_TEXT, attendanceReport.getBody())
    }
    val shareIntent = Intent.createChooser(intent, null)
    context.startActivity(shareIntent)
}