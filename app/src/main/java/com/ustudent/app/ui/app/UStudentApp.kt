package com.ustudent.app.ui.app

import androidx.compose.material.DrawerValue
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ModalDrawer
import androidx.compose.material.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.graphics.Color
import com.ustudent.app.designsystem.theme.DrawerShape
import com.ustudent.app.ui.app.component.UStudentDrawer
import kotlinx.coroutines.launch

@Composable
fun UStudentApp(
    appState: UStudentAppState = rememberUStudentAppState()
) {
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    ModalDrawer(
        drawerState = drawerState,
        drawerContent = {
            UStudentDrawer(
                notificationCount = 3,
                drawerItems = appState.topLevelDestinations,
                currentItem = appState.currentDestination,
                onNavigateToDestination = { dest ->
                    appState.navigate(dest)
                    scope.launch {
                        drawerState.close()
                    }
                },
                quote = "Цитата",
                onQuoteLike = { id, liked ->

                },
                onSuggestQuote = {

                }
            )
        },
        scrimColor = Color.Black.copy(alpha = 0.32f),
        drawerShape = DrawerShape,
        drawerBackgroundColor = MaterialTheme.colors.background
    ) {
        UStudentNavHost(
            navController = appState.navController,
            onNavigationIconClick = {
                scope.launch { drawerState.open() }
            }
        )
    }
}