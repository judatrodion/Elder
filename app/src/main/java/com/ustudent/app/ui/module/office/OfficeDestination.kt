package com.ustudent.app.ui.module.office

import com.ustudent.app.R
import com.ustudent.app.ui.base.navigation.UStudentNavigationDestination

object OfficeDestination : UStudentNavigationDestination {
    override val route: String = "office_destination"
    override val textId: Int
        get() = R.string.personal_account
}