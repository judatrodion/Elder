package com.ustudent.app.ui.module.home.screen.attendance.model

import androidx.annotation.StringRes
import com.ustudent.app.R

enum class AttendanceFilter(@StringRes val stringRes: Int) {
    ALL(R.string.all),
    ATTEMPTING(R.string.attempting),
    MISSING(R.string.missing)
}