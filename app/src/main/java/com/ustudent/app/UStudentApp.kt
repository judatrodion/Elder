package com.ustudent.app

import android.app.Application
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class UStudentApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initPreferences()
    }

    private fun initPreferences() {
        Hawk.init(this).build()
    }
}