package com.ustudent.app

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.core.view.WindowCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.ustudent.app.designsystem.theme.DarkThemeColors
import com.ustudent.app.designsystem.theme.ElderTheme
import com.ustudent.app.ui.app.UStudentApp
import com.ustudent.app.ui.module.login.LoginModule
import com.ustudent.app.ui.module.splash.SplashScreen
import com.ustudent.app.ui.utils.ElderScreens
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            UStudentLaunch()
        }
    }
}


@Composable
fun UStudentLaunch(viewModel: MainViewModel = hiltViewModel()) {
    SetAppBarColor()
    ElderTheme {
        val navController = rememberNavController()
        val destination by viewModel.screenLD.collectAsState()
        LaunchedEffect(destination) {
            navController.popBackStack()
            navController.navigate(destination.route) {
                popUpTo(navController.graph.findStartDestination().id) {
                    inclusive = true
                }
            }
        }
        NavHost(
            navController = navController,
            startDestination = ElderScreens.Splash.route,
            modifier = Modifier
                .fillMaxSize()
                .statusBarsPadding()
                .navigationBarsPadding()
        ) {
            composable(ElderScreens.Login.route) {
                LoginModule {
                    viewModel.launchHome()
                }
            }
            composable(ElderScreens.Home.route) {
                UStudentApp()
            }
            composable(ElderScreens.Splash.route) {
                SplashScreen()
            }
        }
    }
}


@Composable
fun SetAppBarColor() {
    val systemUiController = rememberSystemUiController()
    val isDarkTheme = isSystemInDarkTheme()
    SideEffect {
        systemUiController.setStatusBarColor(
            color = if (isDarkTheme) DarkThemeColors.background else Color.Black,
            darkIcons = false
        )
        systemUiController.setNavigationBarColor(
            color = if (isDarkTheme) DarkThemeColors.background else Color.Black
        )
    }
}
